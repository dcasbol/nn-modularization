#!/bin/bash

for i in {3..15}
do
	python generalization_range.py modular $i | tail -1 >> generalization_test.txt
done
