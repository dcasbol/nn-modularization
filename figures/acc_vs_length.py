import sys
import os
import matplotlib.pyplot as plt

def get_data(train_length):

	acc_list = list()

	with open('./gen_data/generalization_{}.txt'.format(train_length)) as fd:
		len_start, len_end = map(int, fd.readline()[:-1].split(','))
		acc_list = [ 1. - float(line[1:line.find(',')]) for line in fd ]

	return len_start, len_end, acc_list

def main():

	len_start, len_end, acc_list = get_data(10)

	plt.figure()
	plt.bar(range(len_start, len_end+1), acc_list, color='black')
	plt.show()

if __name__ == '__main__':
	sys.exit(main())