"""
references:
https://matplotlib.org/1.4.0/examples/specialty_plots/hinton_demo.html
"""
import sys
import os
import json
import argparse
import numpy             as np
import cPickle           as pickle
import matplotlib.pyplot as plt
plt.rcParams["font.family"] = "FreeSerif"

sys.path.append('./')
get_train_len = lambda i: 3+i
get_test_len  = lambda i, l: l+i
MIN_TRAIN_LEN = 3
MAX_TRAIN_LEN = 15

def default_params(test_len):
	import utils
	return dict(
		batch_size = 100,
		max_seq_len = test_len,
		max_ops_applied = utils.max_ops(test_len),
		randomize_input_length = False,
		run_train = False,
		resume = True,
		save = False,
	)

def get_hinton_squares(matrix):
	"""Draw Hinton diagram for visualizing a weight matrix."""
	ax = plt.gca() # Get current axes

	ax.patch.set_facecolor('white')
	ax.set_aspect('equal', 'box')
	ax.xaxis.set_major_locator(plt.NullLocator())
	ax.yaxis.set_major_locator(plt.NullLocator())

	for (y, x), w in np.ndenumerate(matrix):
		
		# Grey frame for each block
		grey_rect = plt.Rectangle([x-0.5, y-0.5], 1., 1.,
			facecolor='none', edgecolor='grey')
		
		# Fill rectangle
		if np.isnan(w):
			size   = 1.
			kwargs = dict(hatch='//')
			edgec  = 'grey'
			color  = 'white'
		else:
			size   = 1. - w
			kwargs = {}
			edgec  = 'black'
			color  = 'black'

		offset = size * 0.5
		center = [x - offset, y - offset]
		rect   = plt.Rectangle(center, size, size,
			facecolor=color, edgecolor=edgec, **kwargs)
		ax.add_patch(rect)

		# grey frame on top
		ax.add_patch(grey_rect)

	ax.autoscale_view()
	ax.invert_yaxis()
	return ax

def restore_ckpt_folder(dirname, train_len, run=None):

	clname = dirname
	if '_' in clname:
		clname = clname[:4]

	run_dir = '' if run is None else 'run-{}/'.format(run)

	std_dir = './checkpoints/{}'.format(clname)
	src_dir = './checkpoints/{0}-bak/{2}{0}-len_{1}'.format(dirname, train_len, run_dir)

	if not os.path.exists(src_dir):
		err_str = """Dir "{}" doesn't exist.""".format(src_dir)
		raise IOError(err_str)

	import shutil
	shutil.rmtree(std_dir, ignore_errors=True)
	shutil.copytree(src_dir, std_dir)
	return

def test(to_restore, train_len, test_len, run=None):

	import tensorflow as tf
	is_modular = type(to_restore) != str
	if not is_modular:
		import monolithic
		to_restore = [to_restore]
		cl = monolithic.MonolithicTraining
	else:
		import ensembled
		cl = ensembled.EnsembledTraining

	import op_modules
	import imp
	imp.reload(op_modules)

	for name in to_restore:
		restore_ckpt_folder(name, train_len, run)

	tf.reset_default_graph()

	kwargs = default_params(test_len)
	if is_modular:
		del kwargs['resume']

	train_class = cl(**kwargs)
	train_class.run()
	return train_class.test_error

def test_monolithic(train_len, test_len):
	results = np.zeros(5)
	for i in range(5):
		results[i] = test('FullModel', train_len, test_len, run=i+1)
	return np.amean(results)

def test_modular(train_len, test_len):
	import op_modules as om
	names = [ m.NAME.lower() for m in [om.MovePtrA, om.MovePtrB, om.ResetPtrB, om.SwapAB, om.ControllerModule] ]
	return test(names, train_len, test_len)

def test_modular_noise(train_len, test_len):
	import op_modules as om 
	names = [ m.NAME.lower()+'_noise' for m in [om.MovePtrA, om.MovePtrB, om.ResetPtrB, om.SwapAB, om.ControllerModule] ]
	return test(names, train_len, test_len)

def set_data_file(file_path, mat):
	with open(file_path, 'wb') as fd:
		pickle.dump(mat, fd, -1)

def get_data_file(file_path):

	n_trained = MAX_TRAIN_LEN - MIN_TRAIN_LEN + 1
	n_test    = 13

	mat = np.full([n_trained, n_test], np.nan)

	if os.path.exists(file_path):

		with open(file_path) as fd:
			load_mat = pickle.load(fd)

		if load_mat.shape == mat.shape:
			return load_mat

		if load_mat.shape[0] <= mat.shape[0] and load_mat.shape[1] <= mat.shape[1]:
			mat[:load_mat.shape[0],:load_mat.shape[1]] = load_mat
			return mat

		if load_mat.shape[0] >= mat.shape[0] or load_mat.shape[1] >= mat.shape[1]:
			mat = load_mat[:mat.shape[0],:mat.shape[1]]

	return mat

def show_matrix(matrix):

	values = matrix[np.logical_not(np.isnan(matrix))]
	assert np.amin(values) >= 0.0 and np.amax(values) <= 1.0, "Values must be in range [0,1]"

	train_len = range(3,3+matrix.shape[0])
	test_len  = [ '+{}'.format(n) for n in range(matrix.shape[1]) ]

	ax = get_hinton_squares(matrix)

	plt.title('Generalization accuracy (%)')
	plt.xlabel('Testing length')
	plt.ylabel('Maximal training length')

	# X ticks
	ind = np.arange(len(test_len))
	tick_marks = [ str(length) for length in test_len ]
	ax.set_xticks(ind)
	tick_names = ax.set_xticklabels(tick_marks)
	plt.setp(tick_names, fontsize=10)

	# Y ticks
	ind = np.arange(len(train_len))
	tick_marks = [ str(length) for length in train_len ]
	ax.set_yticks(ind)
	tick_names = ax.set_yticklabels(tick_marks)
	plt.setp(tick_names, fontsize=10)

	plt.show()

def fill_test_data(data, test_fn, file_path):

	finished = False
	new_data = np.copy(data)

	try:
		h, w = data.shape
		for i in range(h):
			train_len = get_train_len(i)
			for j in range(w):
				if j >= 2:
					lastvals = np.amin(new_data[i,j-2:j])
				 	if np.isnan(lastvals) or lastvals > 0.95:
						continue
				if np.isnan(new_data[i,j]):
					test_len = get_test_len(j, train_len)
					err = test_fn(train_len, test_len)
					new_data[i,j] = err
					set_data_file(file_path, new_data)
					print '{}/{} --> {}'.format(train_len, test_len, err)
		finished = True
	except KeyboardInterrupt:
		pass

	return finished, new_data

def run_all_tests(testmode, dont_show):

	path_pattern = './figures/gen_data/gen_{}.dat'

	# Initialize data file if not existing
	file_path = path_pattern.format(testmode)

	data = get_data_file(file_path)

	test_fn_dict = {
		'monolithic'    : test_monolithic,
		'modular'       : test_modular,
		'modular-noise' : test_modular_noise
	}
	test_fn = test_fn_dict[testmode]

	finished, data = fill_test_data(data, test_fn, file_path)
	if finished and not dont_show:
		show_matrix(data)

def main():
	os.chdir('../')

	parser = argparse.ArgumentParser(description='Compute the generalization plot graph.')
	parser.add_argument('testmode',
		choices=['monolithic', 'modular', 'modular-noise'],
		help='Select the test to run i.e. the model to load.')
	parser.add_argument('--dont-show', action='store_true',
		help="Only save data file. Don't show the graph plot")
	args = parser.parse_args()

	args.dont_show = False
	run_all_tests(args.testmode, args.dont_show)

if __name__ == '__main__':
	sys.exit(main())
