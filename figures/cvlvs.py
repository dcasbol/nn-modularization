import sys
import argparse
import figutils
import cPickle           as pickle
import numpy             as np
import matplotlib.pyplot as plt

plt.rcParams["font.family"] = "FreeSerif"

def get_plot_data(logfile):

	with open(logfile) as fd:
		d = pickle.load(fd)['log']

	times  = np.array(d['time'])
	times -= times[0]

	low = lambda x: figutils.low_res(x, factor=25)

	out_acc = low(np.array(d['Output failures']))
	trc_acc = low(np.array(d['Trace failures']))
	acc = np.amax([out_acc, trc_acc], axis=0)

	return low(times), acc

def main():

	parser = argparse.ArgumentParser(description='Comparison CV Learning vs. Common training')
	parser.add_argument('length', type=int)
	args = parser.parse_args()

	LENGTH = args.length

	smooth_fn = lambda x: figutils.mv_avg(x)

	logfile = '../logs/len_{}-cvl.log.dat'.format(LENGTH)
	times_cvl, loss_cvl = get_plot_data(logfile)
	loss_cvl_mv = figutils.smoothed(loss_cvl)

	logfile = '../logs/len_{}-no_cvl.log.dat'.format(LENGTH)
	times_all, loss_all = get_plot_data(logfile)
	loss_all_mv = figutils.smoothed(loss_all)

	plt.figure()
	plt.title('Training progress (max. length {})'.format(LENGTH))
	plt.xlabel('Wall-clock time (seconds)')
	plt.ylabel('Combined error rate')

	#plt.plot(times_cvl, loss_cvl, color='black', alpha=0.3)
	plt.plot(times_cvl, loss_cvl_mv, color='black', label='Curriculum Learning')

	#plt.plot(times_all, loss_all, color='black', alpha=0.3, linestyle=':')
	plt.plot(times_all, loss_all_mv, color='black', linestyle=':', label='Standard Training')

	plt.legend()
	plt.show()

if __name__ == '__main__':
	sys.exit(main())