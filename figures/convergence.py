import sys
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np
plt.rcParams["font.family"] = "FreeSerif"

"""
Take ref here:
https://matplotlib.org/examples/pylab_examples/hatch_demo.html
http://people.duke.edu/~ccc14/pcfb/numpympl/MatplotlibBarPlots.html
"""

def main(parallel):

	length_range = range(3, 10+1)

	# For monolithic, average and stddev over 5 runs
	runs_mono = list()
	for run_idx in range(1,5+1):

		# Training times for monolithic
		times_mono = list()
		for input_length in length_range:

			logfile = '../logs/monolithic/run-{}/len_{}.log.dat'.format(run_idx, input_length)
			with open(logfile) as fd:
				d = pickle.load(fd)['log']

			times_mono.append(d['time'][-1] - d['time'][0])

		runs_mono.append(times_mono)

	times_mono = np.array(runs_mono)
	times_mono_mean = np.mean(times_mono, axis=0)
	times_mono_std  = np.std(times_mono, axis=0)

	# Training times for modular
	times_modular = [0.]*len(length_range)
	name_list = ['ctrl', 'swap', 'ptra', 'ptrb', 'rstb']

	for i, input_length in enumerate(length_range):
		for name in name_list:

			logfile = '../logs/modular/noise/{0}/{0}_noise-len_{1}.log.dat'.format(name, input_length)
			with open(logfile) as fd:
				d = pickle.load(fd)['log']

			t = d['time'][-1] - d['time'][0]

			if parallel:
				times_modular[i] = max(times_modular[i], t)
			else:
				times_modular[i] += t

	times_modular = np.array(times_modular)

	# Actual plot
	width = 0.35
	ind   = np.arange(len(length_range))

	fig = plt.figure()
	ax  = fig.add_subplot(111)

	ax.set_title('Model convergence' + ' (parallel)'*int(parallel))
	ax.set_xlabel('Maximal input length')
	ax.set_ylabel('Time to converge (seconds)')
	ax.set_yscale('log')

	bars_mono = ax.bar(ind, times_mono_mean, width,
		yerr=times_mono_std,
		label='Monolithic', edgecolor='black', color='white', hatch='//')

	bars_modu = ax.bar(ind+width, times_modular, width, label='Modular', color='black')

	tick_marks = [ str(length) for length in length_range ]
	ax.set_xticks(ind+width*0.5)
	tick_names = ax.set_xticklabels(tick_marks)
	plt.setp(tick_names, fontsize=10)

	plt.legend(loc='upper left')
	plt.show()

if __name__ == '__main__':
	sys.exit(main(len(sys.argv) > 1 and sys.argv[1] == 'parallel'))