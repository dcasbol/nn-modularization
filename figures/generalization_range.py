import os
import sys
import shutil
import argparse
import tensorflow as tf
import numpy as np

def get_custom_class(original_class):
	import utils

	class CustomTrainingClass(original_class):
		def _test_dict(self):
			max_ops_ph = self._max_ops
			max_ops    = utils.max_ops(self._max_len)
			self._max_ops = max_ops
			d = super(CustomTrainingClass, self)._test_dict()
			self._max_ops = max_ops_ph
			d[max_ops_ph] = max_ops
			return d

	return CustomTrainingClass

class GeneralizationTester(object):

	def __init__(self, test_case, train_len):
		assert test_case in {'monolithic', 'modular'}

		self._max_ops_ph = tf.placeholder(tf.int32)
		self._test_case  = test_case
		self._train_len  = train_len

		if test_case == 'monolithic':
			self._to_restore = ['FullModel']
		else:
			import op_modules as om
			classes = [om.MovePtrA, om.MovePtrB, om.ResetPtrB, om.SwapAB, om.ControllerModule]
			self._to_restore = [ m.NAME.lower()+'_noise' for m in classes ]

		self._is_modular = test_case == 'modular'
		self._run = None

		if self._is_modular:
			self._restore_ckpts()

		if not self._is_modular:
			import monolithic
			cl = monolithic.MonolithicTraining
		else:
			import ensembled
			cl = ensembled.EnsembledTraining
		cl = get_custom_class(cl)

		just_some_length = 10
		kwargs = dict(
			batch_size = 100,
			max_seq_len = just_some_length,
			max_ops_applied = self._max_ops_ph,
			randomize_input_length = False,
			run_train = False,
			save = False,
		)
		if not self._is_modular:
			kwargs['resume'] = True

		self._train_class = cl(**kwargs)

	def _restore_ckpts(self):
		for name in self._to_restore:
			self._restore_ckpt_folder(name)

	def _restore_ckpt_folder(self, dirname):

		clname = dirname[:4] if '_' in dirname else dirname

		run_dir = '' if self._run is None else 'run-{}/'.format(self._run)

		std_dir = './checkpoints/{}'.format(clname)
		src_dir = './checkpoints/{0}-bak/{2}{0}-len_{1}'.format(dirname, self._train_len, run_dir)

		if not os.path.exists(src_dir):
			err_str = """Dir "{}" doesn't exist.""".format(src_dir)
			raise IOError(err_str)

		shutil.rmtree(std_dir, ignore_errors=True)
		shutil.copytree(src_dir, std_dir)
		return

	def _run_test_modular(self, test_len):
		self._train_class._max_len = test_len # This is a hack, but well...
		self._train_class.run()
		return 1. - self._train_class.test_error

	def _run_test_monolithic(self, test_len):
		# Optimistic test
		values = np.zeros(5)
		for r in range(5):
			self._run = r+1
			self._restore_ckpts()
			self._train_class._max_len = test_len # Same hack
			self._train_class.run()
			values[r] = 1. - self._train_class.test_error
		return np.amax(values)

	def _run_test(self, test_len):
		if self._is_modular:
			return self._run_test_modular(test_len)
		else:
			return self._run_test_monolithic(test_len)

	def _find_0_acc(self):

		# Start at 2*training_length
		test_len = self._train_len
		acc = 1.
		while acc > 0. and (test_len + self._train_len) < 100:
			test_len += self._train_len
			acc = self._run_test(test_len)

		# Find the exact spot if possible
		if acc == 0.:
			return self._find_0_acc_recursive(self._train_len, test_len)
		else:
			return (test_len, acc)

	def _find_0_acc_recursive(self, min_len, max_len):

		test_len = min_len + (max_len - min_len) // 2
		acc = self._run_test(test_len)

		if acc > 0.:
			if max_len - test_len == 1:
				return (max_len, 0.)
			return self._find_0_acc_recursive(test_len, max_len)
		else:
			if test_len - min_len == 1:
				return (test_len, acc)
			return self._find_0_acc_recursive(min_len, test_len)

	def _find_100_acc(self):
		return self._find_100_acc_recursive(self._train_len, self._0_acc)

	def _find_100_acc_recursive(self, min_len, max_len):

		if max_len - min_len == 1:
			return min_len

		test_len = min_len + (max_len - min_len) // 2
		acc = self._run_test(test_len)

		if acc < 100.:
			if test_len - min_len == 1:
				return min_len
			return self._find_100_acc_recursive(min_len, test_len)
		else:
			if max_len - test_len == 1:
				return test_len
			return self._find_100_acc_recursive(test_len, max_len)

	def get_generalization_range(self):
		self._0_acc, self._0_acc_value = self._find_0_acc()
		if self._0_acc_value < 1.:
			self._100_acc = self._find_100_acc()
		else:
			self._100_acc = self._0_acc

		# Test up to 5 intermediate points
		inter_acc = list()
		n_points = self._0_acc - self._100_acc - 1
		max_points = 5
		if n_points > 0:
			if n_points <= max_points:
				for i in range(n_points):
					test_len = self._100_acc+1+i
					acc = self._run_test(test_len)
					inter_acc.append((test_len, acc))
			else:
				max_len = self._0_acc
				min_len = self._100_acc
				f = 1. / (max_points+1)
				for i in range(max_points):
					test_len = int(min_len + (max_len - min_len)*(i+1)*f)
					acc = self._run_test(test_len)
					inter_acc.append((test_len, acc))

		return self._100_acc, (self._0_acc, self._0_acc_value), inter_acc

def main():
	os.chdir('../')
	sys.path.insert(0, './')

	parser = argparse.ArgumentParser(description='Compute the generalization barplot graph.')
	parser.add_argument('testcase',
		choices=['monolithic', 'modular'],
		help='Select the test to run i.e. the model to load.')
	parser.add_argument('trainlen', type=int,
		help="Length of lists to test")
	args = parser.parse_args()

	tester = GeneralizationTester(args.testcase, args.trainlen)
	print tester.get_generalization_range()

if __name__ == '__main__':
	sys.exit(main())