import sys
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np
from matplotlib.ticker import MaxNLocator

plt.rcParams["font.family"] = "FreeSerif"

def gbar(lengths, values):
	assert len(lengths) == len(values), "gbar: input parameter lengths don't match"

	l = 0
	i = 0
	max_len = lengths[-1]
	X = list()

	while l <= max_len and i < len(lengths)-1:
		s = (l - lengths[i]) / float(lengths[i+1] - lengths[i])
		v = values[i] + (values[i+1] - values[i]) * s
		X.append([v, v])
		l += 1
		if l == lengths[i+1]:
			i += 1

	v = values[-1]
	X.append([v, v])
	X.reverse()

	x0 = lengths[0]-0.4
	x1 = lengths[0]+0.4
	y1 = lengths[-1]
	plt.imshow(X, interpolation='bicubic', cmap=plt.cm.Greys,
                  extent=(x0, x1, 0, y1), alpha=1, vmin=0, vmax=1)

def get_data(suffix, filename=None):
	if filename is None:
		filename = './gen_data/gen_test-{}.txt'.format(suffix)
	with open(filename) as fd:
		data = list()
		for line in fd:
			data.append(eval(line))
	return data

def plot_single(plt_idx, fn=None):

	title  = ['Monolithic', 'Modular'][plt_idx]
	suffix = title.lower()
	data   = get_data(suffix, filename=fn)

	ax = plt.subplot(111,
		title  = title,
		xlabel = 'Maximal trained length',
		ylabel = 'Tested length')

	max_test_len = 0

	# Zero and Full accuracy lines
	for train_len, (zero_len, _), inter_accuracies in data:
		max_len = train_len
		for l, acc in inter_accuracies:
			if acc > 0.9:
				max_len = l
		plt.plot([0, train_len-0.4], [zero_len, zero_len], color='black', alpha=0.25, zorder=0.)
		plt.plot([train_len-0.4, train_len+0.4], [max_len, max_len], color='white', alpha=0.25, linestyle='--', zorder=1.)

	for train_len, (zero_len, zero_acc), inter_accuracies in data:

		max_test_len = max(max_test_len, zero_len)

		inter_len_list = inter_acc_list = []
		if len(inter_accuracies) > 0:
			inter_len_list, inter_acc_list = [ list(x) for x in zip(*inter_accuracies) ]
		len_list = [train_len]+inter_len_list+[zero_len]
		acc_list = [1.]+inter_acc_list+[zero_acc]
		gbar(len_list, acc_list)

		# Zero accuracies
		plt.bar([train_len], [zero_len], edgecolor='black', color=tuple([0.]*4), linestyle=':')

	train_len_list = [ tl for tl, _, _ in data ]
	min_train_len = min(train_len_list)
	max_train_len = max(train_len_list)

	ax.set(xlim=(min_train_len-0.5, max_train_len+0.5), ylim=(0, max_test_len*1.1))
	ax.yaxis.set_major_locator(MaxNLocator(integer=True))
	ax.set_aspect('auto')

def main():

	plt.figure()

	fn = None
	if len(sys.argv) > 2:
		fn = sys.argv[2]

	plot_single(int(sys.argv[1]), fn)

	plt.colorbar(label='Accuracy', orientation='horizontal')
	plt.show()

if __name__ == '__main__':
	sys.exit(main())