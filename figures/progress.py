import sys
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np
import argparse

plt.rcParams["font.family"] = "FreeSerif"

"""
Take ref here:
https://matplotlib.org/tutorials/introductory/pyplot.html#sphx-glr-tutorials-introductory-pyplot-py
"""

def main():

	parser = argparse.ArgumentParser(description='Generate training progress graph')
	parser.add_argument('length', type=int)
	parser.add_argument('--xaxis', type=str, default='time', choices=['time', 'steps'],
		help='Select what the X axis represents.')
	args = parser.parse_args()

	LENGTH = args.length
	XAXIS  = args.xaxis

	# Loss vs. elapsed time for monolithic
	logfile = '../logs/monolithic/run-{}/len_{}.log.dat'

	# Take run with least elapsed time
	min_time = -1
	for r in range(1,2+1):
		with open(logfile.format(r, LENGTH)) as fd:
			times = pickle.load(fd)['log']['time']
			t = times[-1] - times[0]
			if min_time < 0 or t < min_time:
				min_time = t
				run_idx  = r

	with open(logfile.format(run_idx, LENGTH)) as fd:
		d = pickle.load(fd)['log']

	times_mono = d['time']
	loss_mono  = d['Output loss']
	err_mono   = d['Output failures']

	# Loss vs. elapsed time for modular
	logfile = '../logs/staged/staged-len_{}.log.dat'.format(LENGTH)
	with open(logfile) as fd:
		d = pickle.load(fd)['log']

	times_staged = d['time']
	loss_staged  = d['Output loss']
	err_staged   = d['Output error']

	# Reduce resolution by 100, as modular tests each 100 steps.
	low_res = lambda x: np.array(x)[np.arange(0,len(x),100)]
	times_staged = low_res(times_staged)
	times_mono, loss_mono, err_mono = map(low_res, [times_mono, loss_mono, err_mono])

	# Normalize times
	times_mono  = np.array(times_mono)
	times_mono -= times_mono[0]

	times_staged  = np.array(times_staged)
	times_staged -= times_staged[0]

	# Calculate averaged curves
	def get_mv(values, decay):
		values_mv = np.copy(values)
		for i in range(1, len(values)):
			values_mv[i] = values_mv[i] * decay + values_mv[i-1] * (1.-decay)
		return values_mv

	loss_mono_mv   = get_mv(loss_mono, 0.01)
	loss_staged_mv = get_mv(loss_staged, 0.25)
	err_mono_mv    = get_mv(err_mono, 0.01)
	err_staged_mv  = get_mv(err_staged, 0.25)

	# Actual plotting
	losses = (loss_mono, loss_mono_mv, loss_staged, loss_staged_mv)
	errors = (err_mono, err_mono_mv, err_staged, err_staged_mv)
	label_loss = 'Log-loss over output list'
	label_err  = 'Combined error rate'

	for label, (vm, vmm, vs, vsm) in [(label_loss, losses), (label_err, errors)]:

		plt.figure()
		plt.title('Training progress')
		if XAXIS == 'time':
			plt.xlabel('Wall-clock time (seconds)')
		else:
			plt.xlabel('Training iterations x100')
		plt.ylabel(label)

		if XAXIS == 'time':
			args_mono = [times_mono, vmm]
			args_staged = [times_staged, vsm]
		else:
			args_mono = [vmm]
			args_staged = [vsm]

		#plt.plot(*tm+[vm],
		#	color='red', alpha=0.3)
		plt.plot(*args_mono, color='black',
			label='Monolithic', linestyle=':')
		#plt.plot(*ts+[vs],
		#	color='blue', alpha=0.3)
		plt.plot(*args_staged, color='black',
			label='Modular')

		plt.legend()
		plt.show()

if __name__ == '__main__':
	sys.exit(main())