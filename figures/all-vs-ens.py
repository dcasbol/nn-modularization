import sys
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np
import argparse
import figutils

plt.rcParams["font.family"] = "FreeSerif"

def main():

	parser = argparse.ArgumentParser(description='Comparison between monolithic and modular training')
	parser.add_argument('length', type=int)
	parser.add_argument('--xaxis', type=str, default='time', choices=['time', 'steps'],
		help='Select what the X axis represents.')
	args = parser.parse_args()

	LENGTH = args.length

	logfile = '../logs/monolithic/run-{}/len_{}.log.dat'

	# Take run with least elapsed time
	min_time = -1
	for r in range(1,2+1):
		with open(logfile.format(r, LENGTH)) as fd:
			times = pickle.load(fd)['log']['time']
			t = times[-1] - times[0]
			if min_time < 0 or t < min_time:
				min_time = t
				run_idx  = r

	with open(logfile.format(run_idx, LENGTH)) as fd:
		d = pickle.load(fd)['log']

	# Loss vs. elapsed time for monolithic
	times_mono = d['time']
	loss_mono  = d['Training loss']
	#err_mono   = np.amax([d['Output failures'], d['Trace failures']], axis=0)
	err_mono   = d['Output failures']

	# Loss vs. elapsed time for ensembled modular
	logfile = '../logs/staged/staged-len_{}.log.dat'.format(LENGTH)
	with open(logfile) as fd:
		d = pickle.load(fd)['log']

	times_modular = d['time']
	loss_modular  = d['Output loss']
	#err_modular   = np.amax([d['Output error'], d['Trace failures']], axis=0)
	err_modular   = d['Output error']

	# Normalize times
	norm_times = lambda times: np.array(times) - times[0]
	times_mono    = norm_times(times_mono)
	times_modular = norm_times(times_modular)

	# Modular training only tests evey 100 steps
	low_res = lambda x: figutils.low_res(x, 20)
	#times_mono, loss_mono, err_mono = map(low_res, [times_mono, loss_mono, err_mono])
	times_modular = figutils.low_res(times_modular)

	# Centered means
	loss_mono_mv    = figutils.smoothed(loss_mono)
	loss_modular_mv = figutils.smoothed(loss_modular, 20)

	err_mono_mv    = figutils.smoothed(err_mono, 200)
	err_modular_mv = figutils.smoothed(err_modular, 20)

	# Actual plotting
	def plot_data(data, label):
		data_mono, data_mono_mv, data_modular, data_modular_mv = data

		plt.figure()
		plt.title('Training progress (max. length {})'.format(LENGTH))
		if args.xaxis == 'time':
			plt.xlabel('Wall-clock time')
		else:
			plt.xlabel('Iterations (x100)')
		plt.ylabel(label)

		if args.xaxis == 'time':
			plt.plot(times_mono, data_mono,
				color='black', alpha=0.3)
			plt.plot(low_res(times_mono), low_res(data_mono_mv), color='black', linestyle=':',
				label='Monolithic')
			plt.plot(times_modular, data_modular,
				color='black', alpha=0.3)
			plt.plot(times_modular, data_modular_mv, color='black',
				label='Modular')
		else:
			#plt.plot(figutils.low_res(data_mono, 100),
			#	color='black', alpha=0.3)
			plt.plot(figutils.low_res(data_mono_mv, 100), color='black', linestyle=':',
				label='Monolithic')
			#plt.plot(data_modular,
			#	color='black', alpha=0.3)
			plt.plot(data_modular_mv, color='black',
				label='Modular')

		plt.legend()
		plt.show()

	plot_data([loss_mono, loss_mono_mv, loss_modular, loss_modular_mv], 'Log-loss over output list')
	plot_data([err_mono, err_mono_mv, err_modular, err_modular_mv], 'Error rate')

if __name__ == '__main__':
	sys.exit(main())