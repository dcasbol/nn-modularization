import numpy as np

# Compute a centered average curve
def smoothed(loss, wsize=50):
	loss_sth = np.copy(loss)
	n = wsize // 2
	for i in range(1, len(loss_sth)):
		i1 = max(0, i-n)
		i2 = i+n
		v1 = loss[i1:i]
		v2 = loss[i:i2]
		loss_sth[i] = np.mean([np.mean(v1), np.mean(v2)])
	return loss_sth

def mv_avg(loss):
	loss_mv = np.copy(loss)
	for i in range(1, len(loss)):
		loss_mv[i] = loss_mv[i] * 0.01 + loss_mv[i-1] * 0.99
	return loss_mv

def low_res(x, factor=100):
	return np.array(x)[np.arange(0,len(x),factor)]