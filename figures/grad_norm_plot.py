import sys
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np
import figutils

plt.rcParams["font.family"] = "FreeSerif"

def plot_curve(fn, label, style):
	with open(fn) as fd:
		d = pickle.load(fd)['log']

	f = 1.
	if label == 'Modular':
		f = 4.

	norm = figutils.smoothed(np.array(d['Gradient norm']), 20) * f
	stddev = figutils.smoothed(np.array(d['Gradient stddev']) / 10., 20) * f

	steps = range(len(norm))
	plt.fill_between(steps, norm+stddev, norm-stddev, color='grey', alpha=0.3)
	plt.plot(steps, norm, color='black', label=label, linestyle=style)

def main():

	assert len(sys.argv) == 3

	plt.figure()
	plt.title('Mean gradient value per weight')
	plt.xlabel('Training step')
	plt.ylabel('Absolute value')

	for fn, label, color in zip(sys.argv[1:], ['Monolithic', 'Modular'], ['--', ':']):
		plot_curve(fn, label, color)

	plt.legend()
	plt.show()

if __name__ == '__main__':
	sys.exit(main())