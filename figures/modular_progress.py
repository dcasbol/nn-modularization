import sys
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np
import argparse
import figutils

plt.rcParams["font.family"] = "FreeSerif"

def main():

	parser = argparse.ArgumentParser(description='Show module convergence wrt. time')
	parser.add_argument('length', type=int)
	args = parser.parse_args()

	LENGTH = args.length

	# Loss vs. elapsed time for ensembled modular
	logfile = '../logs/staged/staged-len_{}.log.dat'.format(LENGTH)
	with open(logfile) as fd:
		d = pickle.load(fd)['log']

	# Only display one pointer loss
	del d['ptrb loss']
	del d['rstb loss']
	del d['Output loss']

	times  = d['time']
	losses = { k: vs for k, vs in d.iteritems() if k[-5:] == ' loss' }

	# Normalize times
	times = np.array(times) - times[0]

	losses_mv = { k: figutils.smoothed(vs) for k, vs in losses.iteritems() }

	# Actual plotting
	plt.figure()
	plt.title('Training progress (max. length {})'.format(LENGTH))
	plt.xlabel('Wall-clock time (seconds)')
	plt.ylabel('Training loss')
	plt.xscale('log')

	styles = ['-', '--', ':']

	for i, (label, values) in enumerate(losses.iteritems()):

		values_mv = losses_mv[label]

		plt.plot(times[:len(values)], values, color='black', alpha=0.3)
		plt.plot(times[:len(values)], values_mv, color='black', label=label[:-5], linestyle=styles[i])

	from matplotlib.font_manager import FontProperties
	font = FontProperties(family='FreeMono')

	plt.legend(prop=font)
	plt.show()

if __name__ == '__main__':
	sys.exit(main())