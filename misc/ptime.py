import cPickle as p
import sys

path = 'training.log.dat'
if len(sys.argv) > 1:
	path = sys.argv[1]

with open(path) as fd:
	d = p.load(fd)

print d['log']['time'][-1]