import sys
import argparse
import utils
import cPickle           as pickle
import matplotlib.pyplot as plt
import numpy             as np

def main():

	parser = argparse.ArgumentParser(description='Plot content of log.dat file')
	parser.add_argument('logfile')
	parser.add_argument('--smooth-factor', type=float, default=0.,
		help='Smooth curves.')
	parser.add_argument('--subscale-factor', type=int, default=1,
		help='Lower resolution by a factor.')
	args = parser.parse_args()

	sf = args.smooth_factor
	assert 0 <= sf and sf < 1, 'smooth-factor has to be in [0,1)'
	subf = args.subscale_factor
	assert subf >= 1, 'Subscale factor has to be >= 1'

	with open(args.logfile) as fd:
		d = pickle.load(fd)

	print '--- Experiment configuration ---'
	max_len = max([ len(pname) for pname in d['config'].iterkeys() ])
	for pname, pvalue in d['config'].iteritems():
		print '{}: {}'.format(pname.ljust(max_len), pvalue)

	training_name = d['config'].get('training_name', None)
	title = 'Training of {}'.format(training_name) if training_name is not None else 'Training'

	for label, value_list in d['log'].iteritems():

		if not utils.ask_if('show "{}"'.format(label)):
			continue

		value_array = np.array(value_list)
		value_array = value_array[np.arange(0, len(value_list), subf)]

		alpha = 0.3 if sf > 0 else 1.
		plt.figure()
		plt.plot(value_array, alpha=alpha)
		plt.title(title)
		subf_label = ' (x{})'.format(subf) if subf > 1 else ''
		plt.xlabel('Iterations' + subf_label)
		plt.ylabel(label)

		if sf > 0:
			for i in xrange(1, len(value_list)):
				value_list[i] = value_list[i]*(1.-sf) + value_list[i-1]*sf
			plt.plot(value_list)

		plt.show()

if __name__ == '__main__':
	sys.exit(main())