import tensorflow as tf

"""
Check this link for further options (as saving modification):
https://github.com/tensorflow/tensorflow/tree/master/tensorflow/python/tools
"""

ckpt_name = './FullModel-len_3/state.ckpt'
r = tf.train.NewCheckpointReader(ckpt_name)
m = r.get_variable_to_shape_map() # dict var_name -> shape
print r.debug_string()
for k in m.iterkeys():
	print r.get_tensor(k)