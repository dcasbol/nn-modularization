import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__)))

import argparse
import utils

def main():
	
	parser = argparse.ArgumentParser(description='Main training script.')
	parser.add_argument('module',
		choices=['ctrl', 'swap', 'ptra', 'ptrb', 'rstb', 'all', 'fine', 'staged'],
		help='Select the module to train, with its respective test configurations.')
	parser.add_argument('--resume', action='store_true',
		help='Resume training from last saved checkpoint.')
	parser.add_argument('--test', action='store_true',
		help='Skip training and run test.')
	parser.add_argument('--learning-rate', type=float, default=1e-3,
		help='Override default learning rate.')
	parser.add_argument('--training-steps', type=float, default=1e10,
		help='Number of training steps (exponential notation allowed).')
	parser.add_argument('--batch-size', type=int, default=32)
	parser.add_argument('--max-list-length', type=int, default=3)
	parser.add_argument('--max-ops', type=int, default=0,
		help='Maximal number of operations applied to the list.')
	parser.add_argument('--dont-save', action='store_true',
		help="Don't save model when training ends.")
	parser.add_argument('--save-plot', action='store_true')
	parser.add_argument('--fixed-list-length', action='store_true',
		help='Use --max-list-length as a fixed length instead of a maximum (Only for attention).')
	#parser.add_argument('--kfold', type=int, default=-1,
	#	help='Train with static dataset, using configuration KFOLD.')
	parser.add_argument('--stop-criteria', type=str, default='',
		help='Logical expression (using and/or and parenthesis) containing {slope, functional, quality}')
	parser.add_argument('--input-noise', action='store_true',
		help='Induce random noise in input one-hots.')
	parser.add_argument('--no-cvl', action='store_true',
		help="Don't use curriculum learning when sampling lists")
	args = parser.parse_args()

	assert args.learning_rate > 0, "learning_rate can't be < 0"
	assert int(args.training_steps) > 0, "training_steps can't be < 0"
	assert args.batch_size > 0, "batch_size can't be < 0"
	assert args.max_list_length > 0, "max_list_length can't be < 0"
	#assert args.kfold >= -1 and args.kfold < 10, "kfold must be in [-1,9]"
	if args.input_noise:
		assert args.module not in {'all', 'fine'}, 'Input noise only available for modules'

	if args.max_ops < 1:
		args.max_ops = utils.max_ops(args.max_list_length)
		print "max-ops set to length's maximum: ", args.max_ops

	# Print actually used values
	print 'Run configuration:'
	for k, v in args._get_kwargs():
		print k.ljust(20), getattr(args, k)

	kwargs = dict(
		learning_rate          = args.learning_rate,
		training_steps         = int(args.training_steps),
		batch_size             = args.batch_size,
		max_seq_len            = args.max_list_length,
		run_train              = not args.test,
		resume                 = args.resume or args.test,
		save                   = not args.dont_save,
		save_plot              = args.save_plot,
		randomize_input_length = not args.fixed_list_length,
		stop_criteria          = args.stop_criteria
	)
	if args.input_noise:
		kwargs['add_training_noise'] = True

	if args.module == 'all':
		from schemes.monolithic import MonolithicTraining
		train_class = MonolithicTraining(
			args.max_ops,
			curriculum_learning = not args.no_cvl,
			**kwargs
		)

	elif args.module == 'ctrl':
		from modules.ctrl import CtrlTraining
		train_class = CtrlTraining(args.max_ops, **kwargs)

	elif args.module == 'fine':
		from schemes.ensembled import EnsembledTraining
		train_class = EnsembledTraining(
			monolithic_training = True,
			max_ops_applied     = args.max_ops,
			**kwargs
		)

	elif args.module == 'staged':
		from schemes.staged import StagedTraining
		train_class = StagedTraining(args.max_ops, **kwargs)

	elif args.module == 'swap':
		from modules.swap import SwapTraining
		train_class = SwapTraining(**kwargs)

	else:
		from modules.pointer import PointerTraining
		train_class = PointerTraining(args.module, **kwargs)

	train_class.run()

if __name__ == '__main__':
	sys.exit(main())