import tensorflow as tf
import os
import utils

LIST, A, B, RESET, ATTENTION = 'list', 'A', 'B', 'reset', 'attention'
NUM_OPERATIONS = len(['ptrA', 'ptrB', 'rstB', 'swap', 'nop'])

class OpModule(object):

	DEFAULT_SAVING_PATH = './checkpoints'
	reuse_variables     = False

	@staticmethod
	def _get_saver(classref, target_path=None):

		assert issubclass(classref, OpModule)

		path = target_path or os.path.join(classref.DEFAULT_SAVING_PATH, 'state.ckpt')

		try:
			saver = classref.saver
		except AttributeError:
			saver = tf.train.Saver(classref.trainable_variables)

		return path, saver

	@staticmethod
	def save(classref, saving_path=None):
		path, saver = OpModule._get_saver(classref, saving_path)
		utils.ensure_dir_exists(path)
		saver.save(tf.get_default_session(), path)

	@staticmethod
	def restore(classref, restoring_path=None):
		path, saver = OpModule._get_saver(classref, restoring_path)
		saver.restore(tf.get_default_session(), path)
		print '({}) weights restored'.format(classref.NAME)

	def __init__(self, inputs, output_size, trainable=True, sequence_length=None):
		""" inputs is a dict: list, A, B """
		assert issubclass(type(self), OpModule) # Abstract class

		self._inputs      = inputs
		self._output_size = output_size
		self._trainable   = trainable
		self._outputs     = inputs.copy()
		self._lstm_fn     = utils.LSTM
		self._initial_state = None
		self._outputs_updated = False
		self._sequence_length = sequence_length

		type(self).DEFAULT_SAVING_PATH = os.path.join(OpModule.DEFAULT_SAVING_PATH, self.NAME.lower())

	def _merged_inputs(self):
		raise NotImplementedError('_merged_inputs not implemented for %s' % self)

	def _outputs_dict(self, merged_logits):
		raise NotImplementedError('_outputs_dict not implemented for %s' % self)

	def _set_trainable_variables(self, trainable_variables):
		classref = type(self)
		
		try:
			variables = classref.trainable_variables
			assert set(variables) == set(trainable_variables)
			status = 'reused'
		except AttributeError:
			classref.trainable_variables = trainable_variables
			classref.reuse_variables = True
			if self._trainable:
				for v in trainable_variables.itervalues():
					tf.add_to_collection('model_trainable_variables', v)
					tf.add_to_collection(self.NAME+'_trainable_variables', v)
			status = 'created'

		print 'Set variables ({}) - {}'.format(self.NAME, status)

	def _update_outputs(self):

		if self._outputs_updated:
			return
		self._outputs_updated = True

		with tf.variable_scope(self.scope) as scope:

			if self.reuse_variables:
				scope.reuse_variables()

			inputs = self._merged_inputs()
			args = ('IntraModuleLSTM', inputs, self._output_size, self.reuse_variables, self._initial_state)
			self._lstm_out = self._lstm_fn(*args, input_length=self._sequence_length)
			self._set_trainable_variables(self._lstm_out['trainable_variables'])
			self._outputs = self._outputs_dict(self._lstm_out['activations'])

	@property
	def logits(self):
		self._update_outputs()
		return self._lstm_out['logits']

	@property
	def outputs(self):
		self._update_outputs()
		return self._outputs

	@property
	def scope(self):

		classref = type(self)

		if not hasattr(classref, '_scope'):
			classref._scope = tf.VariableScope(False, self.NAME)
		
		return classref._scope
	

class MovePtrA(OpModule):

	NAME  = 'ptrA'

	def __init__(self, inputs, trainable=True):
		super(MovePtrA, self).__init__(inputs, 1, trainable=trainable)

	def _merged_inputs(self):
		return self._inputs[A]

	def _outputs_dict(self, activations_list):
		self._outputs[A] = activations_list
		return self._outputs

class MovePtrB(OpModule):

	NAME = 'ptrB'

	def __init__(self, inputs, trainable=True):
		super(MovePtrB, self).__init__(inputs, 1, trainable=trainable)

	def _merged_inputs(self):
		return self._inputs[B]

	def _outputs_dict(self, activations_list):
		self._outputs[B] = activations_list
		return self._outputs

class ResetPtrB(OpModule):

	NAME = 'rstB'

	def __init__(self, inputs, trainable=True):
		super(ResetPtrB, self).__init__(inputs, 1, trainable=trainable)

	def _merged_inputs(self):
		return self._inputs[A]

	def _outputs_dict(self, activations_list):
		self._outputs[B] = activations_list
		return self._outputs

class SwapAB(OpModule):

	NAME = 'swap'

	def __init__(self, inputs, sequence_length, trainable=True):
		super(SwapAB, self).__init__(inputs, 11,
			trainable=trainable, sequence_length=sequence_length)
		self._lstm_fn = utils.biLSTM

	def _merged_inputs(self):
		return utils.merge(self._inputs, [LIST,A,B])

	def _outputs_dict(self, activations_list):
		self._outputs[LIST] = activations_list[:,:,:-1]
		return self._outputs

class ControllerModule(OpModule):

	NAME = 'ctrl'

	def __init__(self, inputs, num_operations, sequence_length=None,
		last_state=None, trainable=True):
		super(type(self), self).__init__(inputs, num_operations,
			trainable=trainable, sequence_length=sequence_length)
		self._initial_state = last_state

	def _merged_inputs(self):
		return utils.merge(self._inputs, [LIST,A,B])

	def _outputs_dict(self, merged_outputs):
		# Take only last step output -> final attention
		return {ATTENTION: merged_outputs[-1]}

	def _update_outputs(self):
		# Controller module has a two layer configuration

		if self._outputs_updated:
			return
		self._outputs_updated = True

		with tf.variable_scope(self.scope) as scope:

			if self.reuse_variables:
				scope.reuse_variables()

			# Produce list embedding
			OUTPUT_LSTM_H = -1 # Bypass LSTM state
			inputs = self._merged_inputs()
			args = ('IntraModuleLSTM-list_embedding', inputs, OUTPUT_LSTM_H, self.reuse_variables)

			embedding_out = utils.LSTM(*args, input_length=self._sequence_length)
			# Not logits actually, as it's the LSTM's "memory-bus"
			embedding = [embedding_out['last_logits']] # sequence of length 1

			# Generate attention vector, regarding embedding and last state
			args = ('IntraModuleLSTM-attention', embedding, self._output_size, self.reuse_variables, self._initial_state)
			self._lstm_out = self._lstm_fn(*args)

		# Set variables and configure output
		trainable_variables = embedding_out['trainable_variables']
		for name, var in self._lstm_out['trainable_variables'].iteritems():
			assert not trainable_variables.has_key(name)
			trainable_variables[name] = var
		self._set_trainable_variables(trainable_variables)
		self._outputs = self._outputs_dict(self._lstm_out['activations'])

	@property
	def logits(self):
		self._update_outputs()
		return self._lstm_out['last_logits']

	@property
	def last_state(self):
		self._update_outputs()
		return self._lstm_out['last_state']
