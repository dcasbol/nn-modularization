import tensorflow as tf
import operations as om
import numpy      as np
import utils
import data

from schemes.trainscheme import TrainingScheme

def ctrl_loop(input_trace, a_trace, b_trace, input_len):

	batch_size = tf.shape(input_len)[0]
	max_ops    = tf.shape(input_trace)[0]

	def step_cond(t, *args):
		return tf.less(t, max_ops)

	def step_body(t, last_state, all_logits, all_outputs):

		input_oh = input_trace[t]
		a_oh     = a_trace[t]
		b_oh     = b_trace[t]

		S_in = {
			om.LIST : input_oh,
			om.A    : a_oh,
			om.B    : b_oh
		}

		model = om.ControllerModule(S_in, om.NUM_OPERATIONS, input_len,
			last_state = last_state)

		state   = model.last_state
		logits  = model.logits
		outputs = model.outputs['attention']

		new_logits  = tf.concat([all_logits, [logits]], axis=0)
		new_outputs = tf.concat([all_outputs, [outputs]], axis=0)

		return t+1, state, new_logits, new_outputs

	# Inner loop launch
	t0           = tf.constant(0)
	zero_state   = utils.initial_lstm_state(batch_size, 100)
	zero_len_seq = tf.zeros([0, batch_size, om.NUM_OPERATIONS])

	state_shape  = utils.lstm_state_shape(zero_state)
	seq_shape    = tf.TensorShape([None, None, om.NUM_OPERATIONS])
	logits_shape = tf.TensorShape([None, om.NUM_OPERATIONS])

	_, _, logits_seq, attention_seq = tf.while_loop(
		step_cond, step_body,
		[t0, zero_state, zero_len_seq, zero_len_seq],
		shape_invariants = [t0.get_shape(), state_shape, seq_shape, seq_shape]
	)

	return logits_seq, attention_seq

class CtrlTraining(TrainingScheme):

	NAME = 'Controller module'

	def __init__(self, max_ops_applied, add_training_noise=False, **kwargs):
		self._max_ops = max_ops_applied
		self._classref = om.ControllerModule
		self._add_training_noise = add_training_noise
		self._last_acc = 0.
		super(CtrlTraining, self).__init__(**kwargs)

	def _set_up_model(self):

		batch_size_dyn = max_len_dyn = max_ops_dyn = None

		# Use these only as placeholders
		self._input_len       = tf.placeholder(tf.int64, [batch_size_dyn])
		self._input_trace_int = tf.placeholder(tf.int64, [max_ops_dyn, max_len_dyn, batch_size_dyn])
		self._a_trace_int     = tf.placeholder(tf.int64, [max_ops_dyn, batch_size_dyn], name='a_tr_ph')
		self._b_trace_int     = tf.placeholder(tf.int64, [max_ops_dyn, batch_size_dyn], name='b_tr_ph')
		self._op_int_ref      = tf.placeholder(tf.int64, [max_ops_dyn, batch_size_dyn], name='op_ph')

		# Use these for input to operations
		input_len       = utils.assert_dim_eq(self._input_len, self._input_trace_int, 0, 2)
		input_trace_int = utils.assert_dim_eq(self._input_trace_int, self._a_trace_int, 2, 1)
		a_trace_int     = utils.assert_dim_eq(self._a_trace_int, self._b_trace_int, 1)
		b_trace_int     = utils.assert_dim_eq(self._b_trace_int, self._op_int_ref, 1)
		op_int_ref      = self._op_int_ref

		max_ops = tf.shape(input_trace_int)[0]
		max_len = tf.shape(input_trace_int)[1]
		input_trace_oh = tf.one_hot(input_trace_int, 10)
		a_trace_oh     = tf.reshape(tf.one_hot(a_trace_int, max_len, axis=1), [max_ops, max_len, -1, 1])
		b_trace_oh     = tf.reshape(tf.one_hot(b_trace_int, max_len, axis=1), [max_ops, max_len, -1, 1])

		# Noise tensors
		self._noise_mag = tf.constant(0.)
		with_noise = lambda x: utils.with_noise(x, self._noise_mag)

		input_tr_noisy = utils.with_noise_softmax(input_trace_oh, self._noise_mag)
		a_tr_noisy     = with_noise(a_trace_oh)
		b_tr_noisy     = with_noise(b_trace_oh)

		logits_seq, attention_seq = ctrl_loop(input_tr_noisy, a_tr_noisy, b_tr_noisy, input_len)

		self._loss = utils.trace_loss(op_int_ref, logits_seq)

		actions     = tf.argmax(logits_seq, axis=2)
		trace_count = utils.trace_count(op_int_ref, actions)
		trace_qual  = utils.certainty_violation_softmax(attention_seq)

		self._test_tensors = [ self._loss, trace_count ]

		# Set of losses to log and to show in terminal
		self._log_tensors_dict = {
			'Training loss'    : self._loss,
			'Trace failures'   : trace_count,
			'Trace quality'    : trace_qual
		}
		self._show_loss = [ self._loss, trace_count ]

	def _check_stop_criteria(self, log_dict):
		self._last_acc *= 0.99
		self._last_acc += 0.01 * (1.-log_dict['Trace failures'])
		return super(CtrlTraining, self)._check_stop_criteria(log_dict)

	def _next_batch(self):
		batch = data.next_batch_controller(
			self._batch_size, self._max_ops, self._max_len,
			randomize_length = self._rnd_length,
			accuracy         = self._last_acc
		)
		batch_dict = {
			self._input_len       : batch['limits'],
			self._input_trace_int : batch['list_trace'],
			self._a_trace_int     : batch['a_trace'],
			self._b_trace_int     : batch['b_trace'],
			self._op_int_ref      : batch['ops_trace']
		}
		if self._add_training_noise:
			batch_dict[self._noise_mag] = self._NOISE_MAG
		return batch_dict

	def _test_dict(self):
		test_dict = self._next_batch()
		return test_dict

	def _visualize(self, test_results):

		print 'Num of failures:'
		print test_results
		return

			

