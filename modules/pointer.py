import tensorflow as tf
import operations as om
import data
import utils

from schemes.trainscheme import TrainingScheme

class PointerTraining(TrainingScheme):

	NAME = 'WARNING: classref not properly set'

	def __init__(self, op_name, add_training_noise=False, **kwargs):
		assert op_name in ['ptra', 'ptrb', 'rstb']
		args = {
			'ptra' : [om.MovePtrA, om.A, om.A, 'pointer A displacement'],
			'ptrb' : [om.MovePtrB, om.B, om.B, 'pointer B displacement'],
			'rstb' : [om.ResetPtrB, om.A, om.B, 'pointer B return']
		}
		self._classref, self._in_ptr_name, self._out_ptr_name, duty = args[op_name]
		self._add_training_noise = add_training_noise
		PointerTraining.NAME = self.NAME = 'Training of module for ' + duty
		super(PointerTraining, self).__init__(**kwargs)

	def _set_up_model(self):

		batch_size_dyn = max_len_dyn = None

		# Inputs
		self._input_ptr      = tf.placeholder(tf.float32, [max_len_dyn, batch_size_dyn, 1])
		self._output_ptr_ref = tf.placeholder(tf.float32, [max_len_dyn, batch_size_dyn, 1])

		self._noise_mag = tf.constant(0.)
		noisy_ptr = utils.with_noise(self._input_ptr, self._noise_mag)

		self._input_dict = {
			self._in_ptr_name : self._input_ptr
		}

		# Shape checks
		output_ptr_ref = utils.assert_dim_eq(self._output_ptr_ref, self._input_ptr, [0,1])

		# Model
		self._model  = self._classref(self._input_dict)
		self._logits = self._model.logits

		self._output_ptr     = self._model.outputs[self._out_ptr_name]
		self._output_ptr_int = utils.ptr_oh_to_int(self._output_ptr)
		self._input_ptr_int  = utils.ptr_oh_to_int(self._input_ptr)
		self._output_ptr_max = tf.reduce_max(self._output_ptr, reduction_indices=0)
		self._errors     = tf.not_equal(self._output_ptr_int, self._input_ptr_int + 1)
		self._num_errors = tf.reduce_sum(tf.cast(self._errors, tf.int64))
		self._count      = utils.mean_failures_ptr(self._output_ptr, output_ptr_ref)
		self._qual       = utils.certainty_violation_ptr(self._output_ptr)

		self._loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(
			labels = output_ptr_ref, logits = self._logits))

		self._test_tensors = [self._output_ptr_int, self._output_ptr_max, self._num_errors, self._input_ptr_int]
		self._show_loss = self._loss
		self._log_tensors_dict = {
			'Training loss' : self._loss,
			'Mean failures' : self._count,
			'Output quality': self._qual
		}

	def _next_batch(self):
		batch, labels = data.next_batch_pointer(self._batch_size, self._max_len)
		batch_dict = {
			self._input_ptr      : batch,
			self._output_ptr_ref : labels
		}
		if self._add_training_noise:
			batch_dict[self._noise_mag] = self._NOISE_MAG
		return batch_dict

	def _test_dict(self):
		batch, _   = data.next_batch_pointer(self._batch_size, self._max_len)
		batch_dict = {
			self._input_ptr : batch
		}
		return batch_dict

	def _visualize(self, test_results):
		o, v, n, i = test_results
		print 'Input index:'
		print i
		print 'Output index:'
		print o
		print 'Output values at index:'
		print v
		print 'Number of errors:'
		print n
