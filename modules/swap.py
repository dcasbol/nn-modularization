import tensorflow as tf
import numpy      as np
import operations as om
import data
import utils

from schemes.trainscheme import TrainingScheme

class SwapTraining(TrainingScheme):

	NAME = 'Training of swap module'

	def __init__(self, add_training_noise=False, **kwargs):
		self._add_training_noise = add_training_noise
		self._classref = om.SwapAB
		super(SwapTraining, self).__init__(**kwargs)

	def _set_up_model(self):

		batch_size_dyn = max_len_dyn = None

		# Inputs
		self._input_len = tf.placeholder(tf.int64, [batch_size_dyn])

		self._input_int      = tf.placeholder(tf.int64, [max_len_dyn, batch_size_dyn])
		self._output_ref_int = tf.placeholder(tf.int64, [max_len_dyn, batch_size_dyn])

		self._A_int = tf.placeholder(tf.int64, [batch_size_dyn])
		self._B_int = tf.placeholder(tf.int64, [batch_size_dyn])

		# Some headache avoidance. Just in case.
		skip_asserts   = True
		assert_dim_eq  = lambda *args, **kwargs: utils.assert_dim_eq(*args, dummy_assert=skip_asserts, **kwargs)
		input_len      = assert_dim_eq(self._input_len, self._input_int, 0, 1)
		output_ref_int = assert_dim_eq(self._output_ref_int, self._input_int)
		A_int          = assert_dim_eq(self._A_int, self._input_int, 0, 1)
		B_int          = assert_dim_eq(self._B_int, self._input_int, 0, 1)

		# Expanded input tensors
		input_oh = tf.one_hot(self._input_int, 10)

		max_len_tensor = tf.shape(self._input_int)[0]
		A_oh = utils.ptr_int_to_oh(A_int, max_len_tensor)
		B_oh = utils.ptr_int_to_oh(B_int, max_len_tensor)

		# Noise tensors
		self._noise_mag = tf.constant(0.)
		with_noise = lambda x: utils.with_noise(x, self._noise_mag)

		self._input_dict = {
			om.LIST : utils.with_noise_softmax(input_oh, self._noise_mag),
			om.A    : with_noise(A_oh),
			om.B    : with_noise(B_oh)
		}

		# Model
		self._model = om.SwapAB(self._input_dict, input_len)
		self._output_oh = self._model.outputs[om.LIST]
		self._output_ref_oh = tf.one_hot(output_ref_int, 10)

		labels = tf.mod(output_ref_int, 11)
		self._loss = tf.losses.sparse_softmax_cross_entropy(labels, self._model.logits)
		#self._loss = tf.losses.log_loss(self._output_ref_oh, self._output_oh)

		self._output_int = utils.list_oh_to_int(self._output_oh)
		self._count      = utils.mean_failures(self._output_int, output_ref_int)
		self._qual       = utils.certainty_violation_softmax(self._output_oh)

		self._test_tensors = [self._input_int, A_int, B_int, self._output_oh, self._count]
		self._show_loss = [self._loss, self._count, self._qual]
		self._log_tensors_dict = {
			'Training loss' : self._loss,
			'Mean failures' : self._count,
			'Output quality': self._qual
		}

	def _initialize(self):
		super(SwapTraining, self)._initialize()
		self._last_acc = 0.

	def _check_stop_criteria(self, log_dict):
		self._last_acc *= 0.99
		self._last_acc += 0.01 * (1.-log_dict['Mean failures'])
		return super(SwapTraining, self)._check_stop_criteria(log_dict)

	def _next_batch(self):
		batch_data = data.next_batch_swap(
			self._batch_size, self._max_len,
			randomize_length = self._rnd_length,
			accuracy = self._last_acc
		)
		batch_len, batch_in, batch_A, batch_B, batch_label = batch_data
		batch_dict = {
			self._input_len      : batch_len,
			self._input_int      : batch_in,
			self._A_int          : batch_A,
			self._B_int          : batch_B,
			self._output_ref_int : batch_label
		}
		if self._add_training_noise:
			batch_dict[self._noise_mag] = self._NOISE_MAG
		return batch_dict

	def _test_dict(self):
		return self._next_batch()

	def _visualize(self, test_results):

		input_list, A_int, B_int, output_list, count = test_results
		input_list, output_list = input_list[:,0], output_list[:,0]
		A_int, B_int = A_int[0], B_int[0]

		print 'Mean failures: ', count

		print 'Input: ',
		numbers = [ str(n) if n >= 0 else '_' for n in input_list ]
		print ','.join(numbers)

		print 'PtrA:  ',
		print '  '*A_int + 'A'

		print 'PtrB:  ',
		print '  '*B_int + 'B'

		print 'Out:   ',
		numbers = [ str(np.argmax(one_hot)) if np.amax(one_hot) > 0.5 else '_' for one_hot in output_list ]
		print ','.join(numbers)

		print 'Output values: argmax -> [min, max]'
		for n, one_hot in zip(numbers, output_list):
			print '{} -> [{}, {}]'.format(n, np.amin(one_hot), np.amax(one_hot))
			