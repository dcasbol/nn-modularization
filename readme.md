# Modularity as a Means for Complexity Management in Neural Networks Learning

This repository contains the source code used to execute the experiments made in the paper with the same title, presented to the AAAI 2019 Spring Symposium on Combining Machine Learning with Knowledge Engineering.

Please cite the original article if you make use of this code.

Run `python train.py -h` to see usage guidelines.

Example command to execute monolithic training with lists up to length 5 and functional and slope stop criteria:

```bash
python train.py all --max-list-length 5 --stop-criteria "functional or slope"
```
