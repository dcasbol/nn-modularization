import tensorflow         as tf
import modules.operations as om
import utils
import re
import time

class TrainingScheme(object):

	def __init__(self,
		batch_size=0, max_seq_len=1, randomize_input_length=True,
		training_steps=int(1e10), learning_rate=1e-3, stop_criteria='',
		run_train=True, run_test=True,
		resume=False, save=True, save_plot=False ):

		assert batch_size > 0, 'batch_size not provided'
		assert max_seq_len > 1, 'max_seq_len not provided'

		self._batch_size = batch_size
		self._max_len    = max_seq_len
		self._tr_steps   = training_steps
		self._lr         = learning_rate
		self._run_train  = run_train
		self._run_test   = run_test
		self._resume     = resume
		self._save_flag  = save
		self._save_plot  = save_plot
		self._do_restore = False
		self._rnd_length = randomize_input_length
		self._stop_criteria = stop_criteria.strip()
		self._t0 = 0.
		self._t_shift = 0.
		self._NOISE_MAG  = 0.4

		# Evaluate logical expression for stop criteria
		if self._stop_criteria != '':
			options = '[(]*(slope|functional|quality)[)]*'
			exp     = '^{0}( (and|or) {0})*$'.format(options)
			assert re.match(exp, self._stop_criteria) is not None, 'stop_criteria expression is not valid'

		self._config_dict = dict(
			training_name  = self.NAME,
			batch_size     = batch_size,
			max_seq_len    = max_seq_len,
			training_steps = training_steps,
			learning_rate  = learning_rate,
			run_train      = run_train,
			run_test       = run_test,
			resume         = resume,
			save           = save,
			randomize_input_length = randomize_input_length
		)

		with tf.device('/cpu:0'):
			self._set_up_model()
			self._step = tf.train.AdamOptimizer(self._lr).minimize(
				self._loss,
				var_list = tf.get_collection('model_trainable_variables')
			)

	def run(self):

		with tf.Session() as self._sess:

			self._initialize()
			if self._resume:
				self._restore()
				self._do_restore = False
			self._train()
			self._test_result = self._test()

	def _train_step(self):

		t0 = time.time()

		batch_dict = self._next_batch()
		self._t_shift += time.time() - t0

		# Only count training time --> step
		t_pre_step = time.time()
		self._sess.run(self._step, feed_dict=batch_dict)
		t_pos_step = time.time()

		log_dict, show_loss = self._sess.run([self._log_tensors_dict, self._show_loss],
			feed_dict=batch_dict)

		t_end = time.time()

		self._t_shift += t_end - t0 - (t_pos_step - t_pre_step)

		return log_dict, show_loss

	def _time(self):
		return time.time() - self._t0 - self._t_shift

	def _train(self):

		if not self._run_train:
			return

		save = self._save_flag

		print 'Training started (ctrl-c to stop)'
		try:
			if not hasattr(self, '_log_dict'):
				self._log_dict = dict(
					time = list()
				)
				for k in self._log_tensors_dict.iterkeys():
					self._log_dict[k] = list()

			self._t0 = time.time()

			for self._i in xrange(self._tr_steps):

				log_dict, show_loss = self._train_step()
				log_dict['time'] = self._time()

				for k in log_dict.iterkeys():
					self._log_dict[k].append(log_dict[k])

				if self._i%100 == 0:
					print self._i, show_loss

				if self._check_stop_criteria(log_dict):
					print 'Training finished (stop criteria)'
					break

		except KeyboardInterrupt:
			print 'Training aborted'
			save = utils.ask_if('save')
			self._do_restore = utils.ask_if('restore')

		if save:
			self._save()

		if self._save_plot:
			self._save_logfile()

		print 'Training finished'

	def _save_logfile(self, filename='training.log.dat'):
		import cPickle as pickle
		d = dict(
			config = self._config_dict,
			log    = self._log_dict
		)
		with open(filename,'wb') as fd:
			pickle.dump(d, fd, -1)

	def _restore_logfile(self, filename='training.log.dat'):
		
		import cPickle as pickle
		with open(filename) as fd:
			d = pickle.load(fd)

		c = d['config']
		for k, v in self._config_dict.iteritems():
			if k not in {'training_name', 'batch_size', 'learning_rate', 'randomize_input_length'}:
				continue
			assert v == c[k]

		self._log_dict = d['log']
		self._i = len(self._log_dict['time'])
		self._t_shift = self._log_dict['time'][-1]
		loss = self._log_dict['Training loss'][-1]
		self._mv_loss  = loss
		self._min_loss = loss

	def _test(self):

		if not self._run_test:
			return

		print 'Running test'
		
		import random
		import numpy
		# Force repeatability
		random.seed(0)
		numpy.random.seed(0)

		if self._do_restore:
			self._restore()
			self._do_restore = False
		test_results = self._sess.run(self._test_tensors, feed_dict=self._test_dict())
		self._test_error = self._get_test_error(test_results)
		self._visualize(test_results)
		return test_results

	def _get_test_error(self, test_results):
		return test_results

	def _initialize(self):
		if self._run_train:
			self._sess.run(tf.global_variables_initializer())
			num_params = self._sess.run(utils.num_of_parameters())
			print 'Initialized model with {} parameters'.format(num_params)
		self._min_criteria_limit = 1000.0
		self._max_criteria_limit = max(
			utils.virtual_epoch(self._max_len, self._batch_size)*0.1,
			self._min_criteria_limit
		)
		self._functional_mv = 1.
		self._quality_mv    = 1.

		keys_with = lambda s: [ k for k in self._log_tensors_dict.keys() if s in k ]
		self._functional_keys = keys_with('failures') + keys_with('error')
		self._quality_keys    = keys_with('quality')

	def _bounded_limit(self):
		limit = (self._i + 1)*0.1
		return max(min(limit, self._max_criteria_limit), self._min_criteria_limit)

	def _restore(self):
		om.OpModule.restore(self._classref)

	def _save(self):
		om.OpModule.save(self._classref)

	def _raise_not_implemented(self, method_name):
		raise NotImplementedError('Method {} not implemented for {}'.format(method_name, str(self)))

	def _set_up_model(self):
		self._raise_not_implemented('_set_up_model')

	def _next_batch(self):
		self._raise_not_implemented('_next_batch')

	def _test_dict(self):
		self._raise_not_implemented('_test_dict')

	def _visualize(self, test_results):
		self._raise_not_implemented('_initialize')

	def _check_stop_criteria(self, log_dict):

		if self._stop_criteria == '':
			return False

		cond = lambda k: k in self._stop_criteria
		slope      = self._check_slope_criteria(log_dict)      if cond('slope')      else None
		functional = self._check_functional_criteria(log_dict) if cond('functional') else None
		quality    = self._check_quality_criteria(log_dict)    if cond('quality')    else None

		crit = eval(self._stop_criteria)
		if crit:
			values = self._stop_criteria
			for i, name in enumerate(['slope', 'functional', 'quality']):
				values = values.replace(name, '{%d}' % i)
			values = values.format(slope, functional, quality)
			print self._stop_criteria
			print values

		return crit

	def _check_functional_criteria(self, log_dict):

		keys = [ k for k in self._functional_keys if log_dict.has_key(k) ]
		assert len(keys) <= 2, "%s" % keys
		
		error = max([ log_dict[k] for k in keys ])

		self._functional_mv *= 0.99
		self._functional_mv += error * 0.01

		if self._i % 100 == 0:
			print 'mean error:', self._functional_mv

		return self._functional_mv < 0.01

	def _check_quality_criteria(self, log_dict):

		keys = [ k for k in self._quality_keys if log_dict.has_key(k) ]
		assert len(keys) == 1, "%s" % keys

		error = log_dict[keys[0]]

		self._quality_mv *= 0.99
		self._quality_mv += error * 0.01

		if self._i % 100 == 0:
			print 'mean quality violation:', self._quality_mv

		return self._quality_mv < 0.01

	def _check_slope_criteria(self, log_dict, plateau_slope=1e-4):

		loss = log_dict['Training loss']

		if self._i == 0:
			self._mv_loss  = loss
			self._min_loss = loss
			self._i_min    = self._i
		else:
			if loss < self._min_loss:
				self._min_loss = loss
				self._i_min    = self._i
			self._mv_loss  = 0.99 * self._mv_loss  + 0.01 * loss

		if self._i_min < 100:
			return False

		slope = abs(self._mv_loss - self._min_loss)

		if self._i % 100 == 0:
			print 'loss slope:', slope

		# When slope isn't steep enough or loss is too little
		return slope < plateau_slope or self._mv_loss < 1e-6

	@property
	def test_error(self):
		return self._test_error
	
