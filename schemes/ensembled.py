import tensorflow as tf
import numpy      as np
import modules.operations as om
import data
import utils
import rnn
import os

from trainscheme import TrainingScheme

class EnsembledTraining(TrainingScheme):

	NAME = 'Ensembled modules finetuning'

	def __init__(self, max_ops_applied=15, freeze_ops=False,
		monolithic_training=False, **kwargs):

		self._max_ops             = max_ops_applied
		self._freeze_ops          = freeze_ops and not monolithic_training
		self._monolithic_training = monolithic_training
		self._nonfine_restored    = False

		super(EnsembledTraining, self).__init__(**kwargs)

	def _set_up_model(self):

		batch_size_dyn = max_len_dyn = None

		self._input_len      = tf.placeholder(tf.int64, [batch_size_dyn])
		self._input_int      = tf.placeholder(tf.int64, [max_len_dyn, batch_size_dyn])
		self._output_ref_int = tf.placeholder(tf.int64, [max_len_dyn, batch_size_dyn])
		self._a_ref_int      = tf.placeholder(tf.int64, [batch_size_dyn])
		self._b_ref_int      = tf.placeholder(tf.int64, [batch_size_dyn])

		input_len      = utils.assert_dim_eq(self._input_len, self._input_int, 0, 1)
		input_int      = utils.assert_dim_eq(self._input_int, self._output_ref_int, [0,1])
		output_ref_int = utils.assert_dim_eq(self._output_ref_int, self._a_ref_int, 1, 0)
		a_ref_int      = utils.assert_dim_eq(self._a_ref_int, self._b_ref_int, 0)
		b_ref_int      = self._b_ref_int

		n_ops = om.NUM_OPERATIONS
		self._op_modules      = [om.MovePtrA, om.MovePtrB, om.ResetPtrB, om.SwapAB]
		self._freezed_modules = self._op_modules if self._freeze_ops else []
		
		self._model = rnn.SortingModel(input_int, self._max_ops, input_len,
			freeze         = self._freezed_modules
		)

		self._trace_int = self._model.trace_target_ph

		# Testing tensors
		actions_log = tf.argmax(self._model.logits, axis=2)
		out_list    = tf.identity(self._model.output[om.LIST])
		max_values  = tf.reduce_max(out_list, reduction_indices=2)

		out_list_int = utils.list_oh_to_int(out_list)

		# Loss function and optimizer
		# Trace
		trace_loss  = utils.trace_loss(self._trace_int, self._model.logits)
		trace_count = utils.mean_failures(self._trace_int, actions_log)
		trace_qual  = utils.certainty_violation(self._model.attention)
		self._loss  = trace_loss

		if self._monolithic_training:

			output_ref_oh = tf.one_hot(output_ref_int, 10)
			max_input_len = tf.shape(input_int)[0]
			a_ref = tf.expand_dims(tf.one_hot(a_ref_int, max_input_len, axis=0), axis=-1)
			b_ref = tf.expand_dims(tf.one_hot(b_ref_int, max_input_len, axis=0), axis=-1)

			merged_ref = tf.concat([output_ref_oh, a_ref, b_ref], 2)
			merged_out = utils.merge(self._model.output, [om.LIST, om.A, om.B])

			output_loss = tf.reduce_mean( tf.losses.log_loss(merged_ref, merged_out) )
			self._loss += output_loss

			output_qual = utils.certainty_violation(merged_out)

		# Display a loss based on number of misordered lists
		output_count = utils.mean_failures(out_list_int, output_ref_int)

		flat = lambda x: tf.reshape(x, [-1])
		self._test_tensors = [output_count, self._loss]

		# Set of losses to log and to show in terminal
		self._log_tensors_dict = {
			'Training loss'    : self._loss,
			'Trace failures'   : trace_count,
			'Trace quality'    : trace_qual
		}
		self._show_loss = [ trace_count, output_count, trace_loss ]
		if self._monolithic_training:
			self._show_loss.extend([output_loss, trace_qual, output_qual])
			self._log_tensors_dict['Output loss']       = output_loss
			self._log_tensors_dict['Output quality']    = output_qual
			self._log_tensors_dict['Output failures']   = output_count

	def _initialize(self):
		super(EnsembledTraining, self)._initialize()

		#Restore everything from common saving checkpoints
		print 'Restoring non fine-tuned modules'
		for classref in [om.ControllerModule] + self._op_modules:
			om.OpModule.restore(classref)

	def _next_batch(self):
		batch = data.next_batch_trace(
			self._batch_size, self._max_ops, self._max_len,
			randomize_length    = self._rnd_length,
			provide_target_list = True
		)
		batch_dict = {
			self._input_int      : batch['seqs'],
			self._input_len      : batch['limits'],
			self._trace_int      : batch['ops'],
			self._output_ref_int : batch['out_seqs'],
			self._a_ref_int      : batch['out_a'],
			self._b_ref_int      : batch['out_b']
		}
		return batch_dict

	def _test_dict(self):
		test_dict = self._next_batch()
		del test_dict[self._trace_int]
		return test_dict

	def _get_path(self, classref):
		dirname = '{}-len_{}-fine'.format(classref.NAME, self._max_len)
		path = os.path.join('./checkpoints', dirname, 'state.ckpt')
		return path

	def _get_test_error(self, test_results):
		return test_results[0]

	def _restore(self):
		""" Restore model from <modname>-len_L-fine. Or w/o fine otherwise """
		print 'Restoring from previous saving point'
		for classref in [om.ControllerModule] + self._op_modules:
			path = self._get_path(classref)
			if not os.path.exists(os.path.dirname(path)):
				path = None
			om.OpModule.restore(classref, path)

	def _save(self):
		""" Save model as fine tuning """
		for classref in [om.ControllerModule] + self._op_modules:
			if classref not in self._freezed_modules:
				path = self._get_path(classref)
				om.OpModule.save(classref, path)

	def _visualize(self, test_results):

		print 'Num of failures:'
		print test_results[:2]
		return

		print 'input list'
		print test_results[2]
		print 'actions_log'
		print test_results[3]
		print 'actions_ovr'
		print test_results[4]

			

