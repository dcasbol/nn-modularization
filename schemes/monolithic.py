import tensorflow as tf
import numpy      as np
import modules.operations as om
import data
import utils
import rnn

from trainscheme import TrainingScheme

class MonolithicTraining(TrainingScheme):
	""" Intended to train the model as a monolithic block """

	NAME = 'Monolithic model'

	def __init__(self, max_ops_applied, curriculum_learning=True, **kwargs):
		self._max_ops  = max_ops_applied
		self._cvl      = curriculum_learning
		self._last_acc = 0.
		super(MonolithicTraining, self).__init__(**kwargs)
		self._set_up_monolithic_saver()

	def _set_up_monolithic_saver(self):
		trainable_variables = tf.get_collection('model_trainable_variables')
		self._saver = tf.train.Saver(
			var_list    = trainable_variables,
			max_to_keep = 1000
		)

	def _set_up_model(self):

		batch_size_dyn = max_len_dyn = None

		self._input_len      = tf.placeholder(tf.int64, [batch_size_dyn])
		self._input_int      = tf.placeholder(tf.int64, [max_len_dyn, batch_size_dyn])
		self._output_ref_int = tf.placeholder(tf.int64, [max_len_dyn, batch_size_dyn])
		self._a_ref_int      = tf.placeholder(tf.int64, [batch_size_dyn])
		self._b_ref_int      = tf.placeholder(tf.int64, [batch_size_dyn])

		output_ref_int = utils.assert_dim_eq(self._output_ref_int, self._input_int)
		input_len      = utils.assert_dim_eq(self._input_len, self._input_int, 0, 1)
		a_ref_int      = utils.assert_dim_eq(self._a_ref_int, self._input_len)
		b_ref_int      = utils.assert_dim_eq(self._b_ref_int, self._input_len)
		
		self._model = rnn.SortingModel(self._input_int, self._max_ops, input_len)

		self._trace_int = self._model.trace_target_ph

		# Testing tensors
		self._actions    = tf.argmax(self._model.logits, axis=2)
		self._out_list   = tf.identity(self._model.output[om.LIST])
		self._max_values = tf.reduce_max(self._out_list, reduction_indices=2)

		self._out_list_int = utils.list_oh_to_int(self._out_list)

		# Loss function and optimizer
		# Trace
		self._trace_loss  = utils.trace_loss(self._trace_int, self._model.logits)
		self._trace_count = utils.mean_failures(self._trace_int, self._actions)
		self._loss = self._trace_loss

		# Output
		output_ref_oh = tf.one_hot(output_ref_int, 10)
		max_input_len = tf.shape(self._input_int)[0]
		a_ref = utils.ptr_int_to_oh(a_ref_int, max_input_len)
		b_ref = utils.ptr_int_to_oh(b_ref_int, max_input_len)

		merged_ref = tf.concat([output_ref_oh, a_ref, b_ref], 2)
		merged_out = utils.merge(self._model.output, [om.LIST, om.A, om.B])

		self._output_loss = tf.losses.log_loss(merged_ref, merged_out)
		self._loss += self._output_loss

		# Display a loss based on number of misordered lists
		self._output_count = utils.mean_failures(self._out_list_int, output_ref_int)
		self._output_qual  = utils.certainty_violation(merged_out)

		self._test_tensors  = [self._output_count, self._loss]
		self._test_tensors += [self._input_int, self._actions, self._out_list_int, self._out_list, self._max_values]

		self._grad_norm_mean, self._grad_norm_stddev = self._get_grads_abs()

		# Set of losses to log and to show in terminal
		self._log_tensors_dict = {
			'Training loss'   : self._loss,
			'Trace failures'  : self._trace_count,
			'Output loss'     : self._output_loss,
			'Output failures' : self._output_count,
			'Output quality'  : self._output_qual,
			'Gradient norm'   : self._grad_norm_mean,
			'Gradient stddev' : self._grad_norm_stddev
		}

		self._show_loss  = [self._trace_count, self._output_count, self._trace_loss]
		self._show_loss += [self._output_loss, self._output_qual]

	def _get_grads_abs(self):

		var_list = tf.get_collection('model_trainable_variables')
		grads = tf.gradients(self._loss, var_list)
		
		flatten = lambda x: tf.reshape(x, [-1])
		grad_list_flat = map(flatten, grads)
		grads_flat = tf.concat(grad_list_flat, 0)
		grads_abs  = tf.abs(grads_flat)
		mean   = tf.reduce_mean(grads_abs)
		stddev = tf.sqrt(tf.reduce_mean(tf.square(grads_abs - mean)))

		return mean, stddev

	def _next_batch(self):
		accuracy = self._last_acc if self._cvl else None
		batch = data.next_batch_trace(
			self._batch_size, self._max_ops, self._max_len,
			randomize_length    = self._rnd_length,
			provide_target_list = True,
			accuracy            = accuracy
		)
		batch_dict = {
			self._input_int      : batch['seqs'],
			self._input_len      : batch['limits'],
			self._trace_int      : batch['ops'],
			self._output_ref_int : batch['out_seqs'],
			self._a_ref_int      : batch['out_a'],
			self._b_ref_int      : batch['out_b']
		}
		if accuracy is not None and self._i % 100 == 0:
			print self._last_acc, batch['limits'][:10]
		return batch_dict

	def _test_dict(self):

		cvl     = self._cvl
		rnd_len = self._rnd_length

		self._cvl = self._rnd_length = False
		test_dict = self._next_batch()
		del test_dict[self._trace_int]

		self._cvl        = cvl
		self._rnd_length = rnd_len

		return test_dict

	def _restore(self):
		self._saver.restore(self._sess, './checkpoints/FullModel/state.ckpt')
		print 'Monolithic model restored'

	def _save(self, monolithic_path=None):
		saving_path = monolithic_path or './checkpoints/FullModel/state.ckpt'
		utils.ensure_dir_exists(saving_path)
		self._saver.save(self._sess, saving_path)
		print 'Monolithic model saved'

	def _check_stop_criteria(self, log_dict):
		self._last_acc *= 0.99
		self._last_acc += 0.01 * (1.-log_dict['Output failures'])
		return super(MonolithicTraining, self)._check_stop_criteria(log_dict)

	def _get_test_error(self, test_results):
		return test_results[0]

	def _visualize(self, test_results):

		print 'Num of failures:'
		print test_results[:2]
		return

		test_results = test_results[2:]
		test_results = [ result[:,0] for result in test_results ]
		input_list, action_list, output_list, output_raw, output_max = test_results

		tostr = lambda n: '_' if n == -1 else str(n)

		print 'Input list:'
		numbers = [ tostr(n) for n in input_list ]
		print ','.join(numbers)

		print 'Output list:'
		numbers = [ tostr(n) for n in output_list ]
		print ','.join(numbers)

		print 'Output list raw and max:'
		print output_raw
		print output_max

		print 'Actions:'
		action_names = ['move_a', 'move_b', 'reset_b', 'swap_ab', 'nop']
		data.exec_trace(input_list, action_list, display=True)

			

