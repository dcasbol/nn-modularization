import tensorflow as tf
import numpy      as np
import modules.operations as om
import modules.ctrl       as ctrl
import utils
import rnn
import data
import time

from trainscheme import TrainingScheme

def _set_module(classref, out_name, out_ref_oh, out_ref_int, *args):

	module = classref(*args)
	module_out = module.outputs[out_name]
	if out_name == om.LIST:
		module_out_int = utils.list_oh_to_int(module_out)
	else:
		module_out_int = utils.ptr_oh_to_int(module_out)

	loss = tf.losses.log_loss(out_ref_oh, module_out)
	err  = utils.mean_failures(module_out_int, out_ref_int)
	qual = utils.certainty_violation(module_out)

	return dict(
		loss    = loss,
		error   = err,
		quality = qual
	)

MODULE_NAMES = ['swap', 'ptra', 'ptrb', 'rstb', 'ctrl']

class StagedTraining(TrainingScheme):
	"""
	Used for training all modules at the same pace, simultaneously.
	At each "large" training step, every module is given a training
	step and then all are ensembled and tested to obtain the
	performance statistics.
	"""

	NAME = 'Simultaneous module training'

	def __init__(self, max_ops, add_training_noise=False, **kwargs):
		self._max_ops = max_ops
		self._add_training_noise = add_training_noise
		super(StagedTraining, self).__init__(**kwargs)

		# Flags to register training stops
		self._stopped = dict(
			swap = False,
			ptra = False,
			ptrb = False,
			rstb = False,
			ctrl = False
		)
		self._criteria_cache = { m : (1., 1.) for m in MODULE_NAMES }
		self._mv_acc = {'swap':0., 'ctrl':0.}

	def _set_up_model(self):

		# TO DO: migrate to int32?

		# Define input placeholders
		self._input_len     = tf.placeholder(tf.int64, [self._batch_size])
		self._ctrl_len      = tf.placeholder(tf.int64, [self._batch_size])
		self._input_int     = tf.placeholder(tf.int64, [self._max_len, self._batch_size])
		self._output_ph_int = tf.placeholder(tf.int64, [self._max_len, self._batch_size])
		self._a_ph_int      = tf.placeholder(tf.int64, [self._batch_size])
		self._b_ph_int      = tf.placeholder(tf.int64, [self._batch_size])
		self._ptr_ph_int    = tf.placeholder(tf.int64, [self._batch_size])

		self._input_trace_int = tf.placeholder(tf.int64, [self._max_ops, self._max_len, self._batch_size])
		self._a_trace_int     = tf.placeholder(tf.int64, [self._max_ops, self._batch_size])
		self._b_trace_int     = tf.placeholder(tf.int64, [self._max_ops, self._batch_size])
		self._op_int_ref      = tf.placeholder(tf.int64, [self._max_ops, self._batch_size])

		# One-hot inputs
		to_pointer = lambda i: utils.ptr_int_to_oh(i, self._max_len)
		
		input_oh  = tf.one_hot(self._input_int, 10)
		a_ph_oh   = to_pointer(self._a_ph_int)
		b_ph_oh   = to_pointer(self._b_ph_int)
		ptr_ph_oh = to_pointer(self._ptr_ph_int)

		# Input noise
		self._noise_mag = tf.constant(0.) # Placeholder with default value = 0
		with_noise = lambda x: utils.with_noise(x, self._noise_mag)

		input_oh_noisy  = utils.with_noise_softmax(input_oh, self._noise_mag)
		a_ph_oh_noisy   = with_noise(a_ph_oh)
		b_ph_oh_noisy   = with_noise(b_ph_oh)
		ptr_ph_oh_noisy = with_noise(ptr_ph_oh)

		# Input dictionaries
		swap_dict = {
			om.LIST : input_oh_noisy,
			om.A    : a_ph_oh_noisy,
			om.B    : b_ph_oh_noisy
		}

		ptra_dict, ptrb_dict, rstb_dict = [ { k : ptr_ph_oh_noisy } for k in [om.A, om.B, om.A] ]

		# Output refs
		ptr_next_int  = self._ptr_ph_int + 1
		ptr_next_oh   = to_pointer(ptr_next_int)
		output_ref_oh = tf.one_hot(self._output_ph_int, 10)

		# Modules train info
		swap_info = _set_module(om.SwapAB, om.LIST, output_ref_oh, self._output_ph_int, swap_dict, self._input_len)
		ptra_info = _set_module(om.MovePtrA, om.A, ptr_next_oh, ptr_next_int, ptra_dict)
		ptrb_info = _set_module(om.MovePtrB, om.B, ptr_next_oh, ptr_next_int, ptrb_dict)
		rstb_info = _set_module(om.ResetPtrB, om.B, ptr_next_oh, ptr_next_int, rstb_dict)
		
		# Modules optimizers
		swap_step = self._build_step(swap_info, om.SwapAB.NAME)
		ptra_step = self._build_step(ptra_info, om.MovePtrA.NAME)
		ptrb_step = self._build_step(ptrb_info, om.MovePtrB.NAME)
		rstb_step = self._build_step(rstb_info, om.ResetPtrB.NAME)

		# Same for controller
		max_len        = self._max_len
		input_trace_oh = tf.one_hot(self._input_trace_int, 10)
		a_trace_oh     = tf.reshape(tf.one_hot(self._a_trace_int, max_len, axis=1), [self._max_ops, max_len, -1, 1])
		b_trace_oh     = tf.reshape(tf.one_hot(self._b_trace_int, max_len, axis=1), [self._max_ops, max_len, -1, 1])

		# Input noise
		input_trace_oh_noisy = utils.with_noise_softmax(input_trace_oh, self._noise_mag)
		a_trace_oh_noisy = with_noise(a_trace_oh)
		b_trace_oh_noisy = with_noise(b_trace_oh)

		args = [input_trace_oh_noisy, a_trace_oh_noisy, b_trace_oh_noisy, self._ctrl_len]
		logits_seq, attention_seq = ctrl.ctrl_loop(*args)

		ctrl_loss = tf.losses.sparse_softmax_cross_entropy(self._op_int_ref, logits_seq)
		ctrl_qual = utils.certainty_violation(attention_seq, red_idxs=[0,2])
		ctrl_step = tf.train.AdamOptimizer(self._lr).minimize(
			ctrl_loss,
			var_list = tf.get_collection(om.ControllerModule.NAME+'_trainable_variables')
		)

		op_int_out = utils.ops_oh_to_int(logits_seq)
		ctrl_err   = utils.mean_failures(op_int_out, self._op_int_ref)

		ctrl_info = dict(
			loss    = ctrl_loss,
			error   = ctrl_err,
			quality = ctrl_qual
		)

		# Structure for managing modules uniformly
		class ManagementInfo:
			pass

		self._management_info = dict()
		for modname in MODULE_NAMES:
			mi = ManagementInfo()
			mi.tensors = locals()['{}_info'.format(modname)]
			mi.step    = locals()['{}_step'.format(modname)]
			if modname in {'swap', 'ctrl'}:
				mi.batcher = getattr(self, '_next_batch_{}'.format(modname))
			self._management_info[modname] = mi

		# Ensemble all modules for testing data
		whole = rnn.SortingModel(self._input_int, self._max_ops, self._input_len)

		out_list = whole.output[om.LIST]
		self._out_loss = tf.losses.log_loss(out_list, output_ref_oh)

		out_list_int  = utils.list_oh_to_int(out_list)
		self._out_err = utils.mean_failures(self._output_ph_int, out_list_int)

		# Instantiate log_tensors_dict with keys that will be used
		self._log_tensors_dict = {
			'Output loss'  : 0,
			'Output error' : 0,
			'error'        : 0,
			'quality'      : 0,
			'Gradient norm': 0,
			'Gradient stddev': 0
		}
		for m in MODULE_NAMES:
			for n in ['loss', 'error', 'quality']:
				self._log_tensors_dict[m+' '+n] = 0

		mean, stddev = self._get_grads_abs()
		self._grad_norm_mean   = mean
		self._grad_norm_stddev = stddev

		# Dummy loss.
		self._loss = ctrl_loss

	def _build_step(self, module_info, module_name):
		step = tf.train.AdamOptimizer(self._lr).minimize(
			module_info['loss'],
			var_list = tf.get_collection(module_name+'_trainable_variables')
		)
		return step

	def _get_grads_abs(self):

		modules = [om.ControllerModule, om.SwapAB, om.MovePtrA, om.MovePtrB, om.ResetPtrB]
		grad_list = list()

		for m in modules:
			var_list = tf.get_collection(m.NAME+'_trainable_variables')
			loss = self._management_info[m.NAME.lower()].tensors['loss']
			grads = tf.gradients(loss, var_list)
			grad_list.extend(grads)
		
		flatten = lambda x: tf.reshape(x, [-1])
		grad_list_flat = map(flatten, grad_list)
		grads_flat = tf.concat(grad_list_flat, 0)
		grads_abs  = tf.abs(grads_flat)
		mean   = tf.reduce_mean(grads_abs)
		stddev = tf.sqrt(tf.reduce_mean(tf.square(grads_abs - mean)))

		return mean, stddev

	def _visualize(self, test_results):
		print test_results

	def _next_batch_swap(self):
		batch_data = data.next_batch_swap(self._batch_size, self._max_len,
			randomize_length = self._rnd_length, accuracy = self._mv_acc['swap'])
		batch_len, batch_in, batch_A, batch_B, batch_label = batch_data
		batch_dict = {
			self._input_len     : batch_len,
			self._input_int     : batch_in,
			self._a_ph_int      : batch_A,
			self._b_ph_int      : batch_B,
			self._output_ph_int : batch_label
		}
		return batch_dict

	def _next_batch_pointer(self, input_ph):
		batch, _ = data.next_batch_pointer(self._batch_size, self._max_len)
		batch = np.argmax(batch, axis=0)
		batch.shape = (self._batch_size,)
		batch_dict = {
			input_ph : batch,
		}
		return batch_dict

	def _next_batch_ctrl(self):
		batch = data.next_batch_controller(
			self._batch_size, self._max_ops, self._max_len,
			randomize_length = self._rnd_length, accuracy = self._mv_acc['ctrl']
		)
		batch_dict = {
			self._ctrl_len        : batch['limits'],
			self._input_trace_int : batch['list_trace'],
			self._a_trace_int     : batch['a_trace'],
			self._b_trace_int     : batch['b_trace'],
			self._op_int_ref      : batch['ops_trace']
		}
		return batch_dict

	def _next_batch_ensembled(self):
		batch = data.next_batch_trace(
			self._batch_size, self._max_ops, self._max_len,
			#randomize_length    = self._rnd_length,
			randomize_length    = False,
			provide_target_list = True
		)
		batch_dict = {
			self._input_int     : batch['seqs'],
			self._input_len     : batch['limits'],
			self._output_ph_int : batch['out_seqs']
		}
		return batch_dict

	def _train_step(self):
		"""
		Run a training step for each module. Show loss of ensembled model.
		"""

		t0 = time.time()

		# Build batch dict
		batch_dict = dict()
		for m in ['swap', 'ctrl']:

			if self._stopped[m]:
				continue

			mi = self._management_info[m]
			bd = mi.batcher()

			for k, v in bd.iteritems():
				batch_dict[k] = v

		learning_pointers = [ m for m in ['ptra', 'ptrb', 'rstb'] if not self._stopped[m] ]
		if len(learning_pointers) > 0:
			batch_dict[self._ptr_ph_int] = self._next_batch_pointer(self._ptr_ph_int)[self._ptr_ph_int]

		if self._add_training_noise:
			batch_dict[self._noise_mag] = self._NOISE_MAG

		# Build training & testing tensors
		learning_modules = [ m for m in MODULE_NAMES if not self._stopped[m] ]

		step_tensors = list()
		log_tensors  = list()
		for m in learning_modules:

			mi = self._management_info[m]
			step_tensors.append(mi.step)
			log_tensors.append(mi.tensors)

		# Run steps
		t_pre_step = time.time()
		self._sess.run(step_tensors, feed_dict=batch_dict)
		t_pos_step = time.time()

		if len(learning_modules) == len(MODULE_NAMES):
			log_tensors.append(self._grad_norm_mean)
			log_tensors.append(self._grad_norm_stddev)
		log_tensors = self._sess.run(log_tensors, feed_dict=batch_dict)

		log_dict = dict()
		for i, m in enumerate(learning_modules):

			for k in ['loss', 'error', 'quality']:
				log_dict[m+' '+k] = log_tensors[i][k]

			if m in {'swap','ctrl'}:
				self._mv_acc[m] *= 0.99
				self._mv_acc[m] += 0.01 * (1. - log_tensors[i]['error'])

			if self._i % 100 == 0:
				print m, '; '.join([ '{}: {}'.format(k, log_tensors[i][k]) for k in ['error', 'quality'] ])
				
			# Check stop criteria
			self._stop_criteria = 'functional' + ' and quality' * int(m != 'ctrl')
			self._functional_mv, self._quality_mv = self._criteria_cache[m]
			self._stopped[m] = super(StagedTraining, self)._check_stop_criteria(log_tensors[i])
			self._criteria_cache[m] = self._functional_mv, self._quality_mv

		log_dict['Gradient norm'], log_dict['Gradient stddev'] = log_tensors[-2:]

		# Ensembled test
		if self._i % 100 == 0:

			self._out_loss_cache = self._out_err_cache = 0.

			iters = int(100.0 / self._batch_size + 0.5)
			for _ in range(iters):
				out_loss, out_err = self._sess.run([self._out_loss, self._out_err],
					feed_dict = self._next_batch_ensembled())
				self._out_loss_cache += out_loss
				self._out_err_cache  += out_err

			self._out_loss_cache /= float(iters)
			self._out_err_cache /= float(iters)

			log_dict['Output loss']  = self._out_loss_cache
			log_dict['Output error'] = self._out_err_cache

		t_end = time.time()
		self._t_shift += t_end - t0 - (t_pos_step - t_pre_step)

		return log_dict, [self._out_loss_cache, self._out_err_cache]

	def _check_stop_criteria(self, log_dict):
		for v in self._stopped.itervalues():
			if not v:
				return False
		return True

	def _test(self):
		# Disabled
		return

	def _save(self):
		# Disabled
		return

	def _restore(self):
		# Disabled
		return