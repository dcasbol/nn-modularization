import tensorflow as tf
import os

####################
#   Miscelaneous   #
####################
expand_dims = lambda x: tf.expand_dims(x, -1)

def max_ops(list_length):
	# s(wap), a(ptr), b(ptr), r(eturnb), n(op)
	assert list_length > 1
	if list_length == 2:
		return len('san')
	return len('sb')*(list_length-2) + len('sar') + max_ops(list_length-1)

def ensure_dir_exists(path):
	dir_path = os.path.dirname(path)
	if not os.path.exists(dir_path):
		os.makedirs(dir_path)

def ask_if(action):
	r = ''
	while r not in ['y','n']:
		r = raw_input('Do you wish to {}? (y/n): '.format(action))
	return r == 'y'

def merge(input_dict, key_list):
	input_list = [ input_dict[k] for k in key_list ]
	return tf.concat(input_list, 2)

def virtual_epoch(max_list_length, batch_size):
	iterations = sum([ 10**n for n in range(2, max_list_length+1) ])
	return iterations / float(batch_size)

def assert_dim_eq(a, b, dim_a=None, dim_b=None, dummy_assert=False):
	""" Check that dimensions DIM_X match and return tensor A with dependencies. 
	DIM_X can be a dimension index or a tuple of such. If DIM_B is not provided,
	it's assumed that is the same as DIM_A. """

	if dummy_assert:
		return a

	_ensure_iterable = lambda x: x if type(x) in [list, tuple] else [x]

	dim_a = _ensure_iterable(dim_a) if dim_a is not None else range(len(a.shape))
	dim_b = _ensure_iterable(dim_b) if dim_b is not None else dim_a
	sa  = tf.shape(a)
	sb  = tf.shape(b)

	dependencies = list()
	for da, db in zip(dim_a, dim_b):
		d = da if da == db else '{}/{}'.format(da, db)
		message   = "Dimension {} of tensors {} and {} must match.".format(d, a, b)
		assert_op = tf.assert_equal(sa[da], sb[db], message = message)
		dependencies.append(assert_op)

	with tf.control_dependencies(dependencies):
		new_a = tf.identity(a)

	return new_a

def get_trace_mask(actions):
	from modules.operations import NUM_OPERATIONS
	nop_op = NUM_OPERATIONS - 1
	is_nop = tf.cast(tf.equal(actions, nop_op), tf.int32)
	mask   = tf.cumsum(is_nop, exclusive=True) # Include first NOP
	mask   = 1 - tf.minimum(mask, 1)
	return mask

def initial_lstm_state(batch_size, num_units):
	from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple
	zeros = lambda: tf.zeros([batch_size, num_units])
	return LSTMStateTuple(zeros(), zeros())

def lstm_state_tuple(c, h):
	from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple
	return LSTMStateTuple(c, h)

def lstm_state_shape(lstm_state):
	from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple
	state_shape = LSTMStateTuple(lstm_state[0].get_shape(), lstm_state[1].get_shape())
	return state_shape

##############################
#   Custom transformations   #
##############################
def _oh_as_index(oh, axis, zero_vectors):
	# Zero vectors are represented as index -1
	index   = tf.argmax(oh, axis=axis)
	if zero_vectors:
		max_val = tf.reduce_max(oh, reduction_indices=axis)
		factor  = tf.cast(tf.round(max_val), tf.int64)
		index   = factor * index + (factor-1)
	return index

def ops_oh_to_int(ops_oh):
	return _oh_as_index(ops_oh, 2, False)

def list_oh_to_int(list_oh):
	return _oh_as_index(list_oh, 2, True)

def ptr_oh_to_int(ptr):
	indices = _oh_as_index(ptr, 0, False)
	return tf.reshape(indices, [-1])

def ptr_int_to_oh(ptr, seq_len):
	return tf.expand_dims(tf.one_hot(ptr, seq_len, axis=0), axis=-1)

def ptr_to_int(ptr):
	print ptr.name
	print 'ptr_to_int is deprecated. Use ptr_oh_to_int'
	return _oh_as_index(ptr, 0, False)

def oh_to_int(oh):
	print oh.name
	print 'oh_to_int is deprecated. Use corresponding <element>_oh_to_int'
	return _oh_as_index(oh, -1, True)

def extend_oh_int(oh):
	last_idx = len(oh.get_shape()) - 1
	ext = 1. - tf.reduce_sum(oh, reduction_indices=last_idx, keep_dims=True)
	return tf.concat([oh, ext], last_idx)

def with_noise(x, noise_mag, skip_assert=False):
	assert skip_assert or int(x.shape[-1]) == 1, 'DEPRECATED: use with_noise_softmax'
	return tf.abs(x - tf.random_uniform(tf.shape(x), maxval=noise_mag))

def with_noise_softmax(x, noise_mag):
	# Add noise to the 11-softmax. Then cut back to 10
	x = extend_oh_int(x)
	x = tf.nn.softmax(with_noise(x, noise_mag, skip_assert=True)*100.0)
	return x[..., :-1]

######################
#   Custom metrics   #
######################
def mean_failures(seq_a, seq_b, red_idx=0):
	misses   = tf.cast(tf.not_equal(seq_a, seq_b), tf.float32)
	failures = tf.minimum(tf.reduce_sum(misses, reduction_indices=red_idx), 1)
	return tf.reduce_mean(failures)

def mean_failures_ptr(seq_oh, ref_oh):
	highs    = tf.cast(tf.greater_equal(seq_oh, 0.5), tf.float32)
	misses   = (1.-highs) * ref_oh + highs * (1.-ref_oh)
	failures = tf.minimum(tf.reduce_sum(misses, reduction_indices=0), 1)
	return tf.reduce_mean(failures)

def certainty_violation_softmax(x, cert=0.9, red_idxs=[0,2]):
	max_val  = tf.reduce_max(x, reduction_indices=red_idxs)
	cert_vlt = tf.cast(tf.less(max_val, cert), tf.float32)
	return tf.reduce_mean(cert_vlt)

def certainty_violation_ptr(x, cert=0.9, red_idxs=[0,2]):
	sat_vals = tf.abs(x-0.5) * 2.
	min_vals = tf.reduce_min(sat_vals, reduction_indices=red_idxs)
	cert_vlt = tf.cast(tf.less(min_vals, cert), tf.float32)
	return tf.reduce_mean(cert_vlt)

def certainty_violation(x, cert=0.9, red_idxs=[0,2]):
	ones  = tf.greater_equal(x, cert)
	zeros = tf.less_equal(x, 1. - cert)
	combi = tf.logical_or(ones, zeros)
	misses = 1. - tf.cast(combi, tf.float32)
	fails  = tf.minimum(tf.reduce_sum(misses, reduction_indices=red_idxs), 1)
	return tf.reduce_mean(fails)

def trace_count(reference, actual):
	mask     = tf.cast(get_trace_mask(reference), tf.float32)
	failures = tf.cast(tf.not_equal(reference, actual), tf.float32)
	failures = failures * mask
	total    = tf.minimum(tf.reduce_sum(failures, reduction_indices=0), 1.)
	return tf.reduce_mean(total)

def trace_loss(labels, logits):
	labels = tf.cast(labels, tf.int32)
	num_steps, batch_size, num_ops = tf.unstack(tf.shape(logits))

	# Only compute up to first NOP
	mask       = get_trace_mask(labels)
	oh_labels  = tf.one_hot(labels, num_ops)
	trace_loss = tf.losses.softmax_cross_entropy(oh_labels, logits, weights=mask)
	return trace_loss

def num_of_parameters():
	var_list = tf.get_collection('model_trainable_variables')
	return tf.reduce_sum(map(tf.size, var_list))

########################
#   Weights & Layers   #
########################
def layer_weights(in_size, out_size):
	W = tf.get_variable('output_weights', [in_size, out_size],
		initializer = tf.truncated_normal_initializer(stddev=1e-4))
	b = tf.get_variable('output_bias', [out_size],
		initializer = tf.zeros_initializer())
	return W, b

def _LSTM_layer(scope_name, inputs, output_size, reuse,
	initial_state = None, input_length = None, bidirectional = False):

	assert output_size > 0 or output_size == -1

	with tf.variable_scope(scope_name, reuse=reuse):

		if type(inputs) == list:
			inputs = tf.identity(inputs)

		time_steps, batch_size, dim_size = tf.unstack(tf.shape(inputs))

		rnn_fn = tf.nn.bidirectional_dynamic_rnn if bidirectional else tf.nn.dynamic_rnn

		kwargs = dict(
			dtype           = tf.float32,
			time_major      = True,
			sequence_length = input_length
		)

		make_cell = lambda: tf.nn.rnn_cell.BasicLSTMCell(100)
		linear    = lambda x, W, b: tf.matmul(x, W) + b

		if bidirectional:
			cell_fw, cell_bw = make_cell(), make_cell()
			args = (cell_fw, cell_bw, inputs)
			flat_shape = [2*time_steps*batch_size, 100]
			full_shape = [2, time_steps, batch_size, output_size]
		else:
			cell_fw = make_cell()
			args = (cell_fw, inputs)
			kwargs['initial_state'] = initial_state
			flat_shape = [time_steps*batch_size, 100]
			full_shape = [time_steps, batch_size, output_size]

		if output_size < 0:
			full_shape[-1] = 100

		outputs, last_state = rnn_fn(*args, **kwargs)
		outputs_flat = tf.reshape(outputs, flat_shape)
		# TODO forward last output all through?

		if output_size > 0:
			# Shared output embedding
			W, b        = layer_weights(100, output_size)
			logits_flat = linear(outputs_flat, W, b)
			logits      = tf.reshape(logits_flat, full_shape)
		else:
			logits = tf.reshape(outputs, full_shape)

		_act_fn = tf.sigmoid if output_size == 1 else tf.nn.softmax
		if bidirectional:
			logits = tf.reduce_sum(logits, reduction_indices=0)
		activations = _act_fn(logits)

		max_length = tf.fill([batch_size], time_steps)
		length     = input_length if input_length is not None else max_length
		batch_idx  = tf.cast(tf.range(batch_size), length.dtype)
		length, batch_idx = expand_dims(length-1), expand_dims(batch_idx)

		indices          = tf.concat([length, batch_idx], 1)
		last_logits      = tf.gather_nd(logits, indices)
		last_activations = tf.gather_nd(activations, indices)

		varname     = lambda name: '_'.join([scope_name, name])
		prefix_list = ['fw_', 'bw_']     if bidirectional else ['']
		cell_list   = [cell_fw, cell_bw] if bidirectional else [cell_fw]

		trainable_variables = dict()
		for prefix, cell in zip(prefix_list, cell_list):
			variables = cell.trainable_variables
			trainable_variables[varname(prefix+'lstm_kernel')] = variables[0] if len(variables) > 0 else []
			trainable_variables[varname(prefix+'lstm_bias')]   = variables[1] if len(variables) > 0 else []

		if output_size > 0:
			trainable_variables[varname('out_weights')] = W
			trainable_variables[varname('out_bias')]    = b

	return dict(
		logits              = logits,
		activations         = activations,
		last_state          = last_state if not bidirectional else None,
		last_logits         = last_logits,
		last_activations    = last_activations,
		trainable_variables = trainable_variables,
	)

def biLSTM(*args, **kwargs):
	return _LSTM_layer(*args, bidirectional=True, **kwargs)

def LSTM(*args, **kwargs):
	return _LSTM_layer(*args, **kwargs)

############################
#   Attention mechanisms   #
############################
def attend_dictionaries(states, attention):

	attention = tf.expand_dims(attention, axis=-1)
	attention_list = tf.unstack(attention, axis=1)
	keys = states[0].keys()

	return { k : attend_sequences([s[k] for s in states], attention_list) for k in keys }

def attend_sequences(sequences, attention_list):

	# attention_list: N x [B]
	# sequences: N x [T,B,D]

	out_seq = list()
	sequences = [ tf.unstack(s) for s in sequences]
	n_steps = len(sequences[0])

	for t in range(n_steps):
		step = sum(a*sequences[i][t] for i, a in enumerate(attention_list))
		out_seq.append(step)

	return out_seq

def attend_dictionaries_by_selection(state_options, attention_logits):

	keys       = state_options[0].keys()
	fst_option = state_options[0].values()[0]
	time_steps, batch_size, _ = tf.unstack(tf.cast(tf.shape(fst_option), tf.int64))

	selection_idx = expand_dims(tf.argmax(attention_logits, axis=1)) # [B,1]
	batch_idx     = expand_dims(tf.range(batch_size)) # [B,1]
	time_idx      = expand_dims(tf.range(time_steps)) # [B,1]
	indices       = tf.concat([selection_idx, batch_idx], 1) # [B,2]

	_attend = lambda seq_options: attend_sequence_by_selection(seq_options, indices)

	return { k : _attend([ s[k] for s in state_options ]) for k in keys }

def attend_sequence_by_selection(sequence_options, indices):
	# seq_options: [N,T,B,D]
	#sequence_options = tf.unstack(sequence_options, axis=1) # -> T x [N,B,D]
	#selection = [ tf.gather_nd(step_options, indices) for step_options in sequence_options ]
	sequence_options = tf.transpose(sequence_options, [1,0,2,3])
	select_fn = lambda x: tf.gather_nd(x, indices)
	selection = tf.map_fn(select_fn, sequence_options)
	# selection item: [N,B,D] -> [B,D]
	return selection # T x [B,D]
