import numpy as np

MIN_LENGTH = 2

def next_batch_pointer(size, steps):

	batch = np.zeros((steps, size, 1))
	idx   = np.random.randint(steps-1, size=size)
	idx_2 = idx + 1

	inputs = batch.copy()
	inputs[idx, np.arange(size), 0] = 1

	labels = batch.copy()
	labels[idx_2, np.arange(size), 0] = 1

	return inputs, labels

def gen_list(batch_size, max_length, randomize_length=True, accuracy=None):

	assert max_length >= MIN_LENGTH

	list_batch = np.random.randint(10, size=(max_length, batch_size))

	if randomize_length and max_length > MIN_LENGTH:
		if accuracy is not None:
			idx = get_idx_from_beta(batch_size, max_length, accuracy)
		else:
			idx = np.random.randint(MIN_LENGTH, high=max_length+1, size=batch_size)
		for b, i in enumerate(idx):
			list_batch[i:,b] = -1
	else:
		idx = np.array([max_length]*batch_size)

	return list_batch, idx

def get_idx_from_beta(batch_size, max_length, acc):
	"""	Draw list lengths from a beta distribution
	that shifts between B(2,1) and B(1,2) depending
	on training accuracy. Thus starts with short
	list lengths and generate long lists at the end
	of the training. """

	a = acc + 1.0
	b = (1.-acc) + 1.0

	p   = np.random.beta(a, b, size=batch_size)
	idx = p * (max_length+1 - MIN_LENGTH) + MIN_LENGTH

	return idx.astype(np.int32)

def gen_trace(sequence, max_steps, seq_len, intermediate_results = False):

	move_a, move_b, reset, swap, nop = range(5)
	seq      = sequence.copy()
	last_pos = seq_len - 1
	trace    = list()

	a = 0
	b = a + 1

	inters     = dict(a = list(), b = list(), l = list())
	add_inters = lambda a, b, l: [ inters[k].append(v) for k, v in zip(['a','b','l'], [a,b,list(l)]) ] if intermediate_results else None
	add_inters(a, b, seq)

	for i in range(max_steps):

		# Stop sorting when A reaches end
		if a == last_pos:
			trace.append(nop)

		# Return B after moving A
		elif trace[-1:] == [move_a]:
			trace.append(reset)
			b = min(a+1, last_pos)

		# Swap digits if unordered
		elif seq[b] < seq[a]:
			trace.append(swap)
			sa, sb = seq[a], seq[b]
			seq[a], seq[b] = sb, sa

		# Move A when B reaches end
		elif b == last_pos:
			trace.append(move_a)
			a = min(a+1, last_pos)

		# Otherwise just go for the next digit
		else:
			trace.append(move_b)
			b = min(b+1, last_pos)

		add_inters(a, b, seq)

	if intermediate_results:
		return [trace] + [ inters[k][:-1] for k in ['l','a','b'] ]

	return trace

def print_state(sequence, a, b, i, op_label, display=True):
	if not display:
		return
	chars = [ '_' if s < 0 else str(s) for s in sequence ]
	i = str(i).ljust(3)+': '
	s = ' '*len(i)
	print i + ','.join(chars), '({})'.format(op_label)
	print s + '  '*a + 'a'
	print s + '  '*b + 'b'
		
def exec_trace(sequence, trace, display = False):

	move_a, move_b, reset, swap, nop = range(5)
	op_labels = ['move_a', 'move_b', 'reset_b', 'swap_ab', 'nop']
	seq = sequence.copy()
	a = 0
	b = a + 1

	print_state(seq, a, b, 0, 'initial', display)
	get_nr = lambda s, i: s[i] if i < len(s) else -1
	def set_nr(s, i, v):
		if i < len(s):
			s[i] = v

	for i, t in enumerate(trace):

		if t == move_a:
			a += 1
		elif t == move_b:
			b += 1
		elif t == reset:
			b = a + 1
		elif t == swap:
			sa, sb = get_nr(seq, a), get_nr(seq, b)
			set_nr(seq, a, sb)
			set_nr(seq, b, sa)

		print_state(seq, a, b, i+1, op_labels[t], display)
	
	return seq, a, b

def next_batch_swap(batch_size, max_len, randomize_length=True, accuracy=None):

	list_batch, limits = gen_list(batch_size, max_len,
		randomize_length = randomize_length, accuracy = accuracy)

	random_idxs = lambda: (np.random.random(batch_size) * limits).astype(np.int64)
	idx_A = random_idxs()
	idx_B = random_idxs()

	list_out  = list_batch.copy()

	batch_idx = np.arange(batch_size)
	list_out[idx_A, batch_idx] = list_batch[idx_B, batch_idx]
	list_out[idx_B, batch_idx] = list_batch[idx_A, batch_idx]

	return limits, list_batch, idx_A, idx_B, list_out

def _is_sorted(seq):
	f    = 0
	last = seq[0]
	for curr in seq[1:]:
		if curr < 0:
			break
		f += int(last > curr)
		last = curr
	return f == 0

def next_batch_controller(size, steps, max_seq_len, randomize_length=True, accuracy=None):
	list_batch, limits = gen_list(size, max_seq_len,
		randomize_length = randomize_length, accuracy = accuracy)
	full_trace = [ gen_trace(list_batch[:,i], steps, limits[i], intermediate_results=True) for i in range(size) ]
	ops_trace, list_trace, a_trace, b_trace = zip(*full_trace)
	list_trace = np.transpose(list_trace, [1,2,0])
	ops_trace, a_trace, b_trace = map(np.transpose, [ops_trace, a_trace, b_trace])
	return dict(
		ops_trace  = ops_trace,
		list_trace = list_trace,
		a_trace    = a_trace,
		b_trace    = b_trace,
		limits     = limits
	)

def next_batch_trace(size, steps, max_seq_len,
	randomize_length=True, provide_target_list=False, accuracy=None):

	list_batch, limits = gen_list(size, max_seq_len,
		randomize_length = randomize_length, accuracy = accuracy)
	traces = [ gen_trace(list_batch[:,i], steps, limits[i]) for i in range(size) ]
	traces = np.stack(traces, axis=1)

	output = dict(
		seqs   = list_batch,
		limits = limits,
		ops    = traces
	)

	if provide_target_list:
		seq_list = list()
		a_list   = list()
		b_list   = list()
		for i in range(size):
			seq, a, b = exec_trace(list_batch[:,i], traces[:,i])
			seq_list.append(seq)
			a_list.append(a)
			b_list.append(b)

		seq_np = np.transpose(np.array(seq_list))
		a_np   = np.array(a_list)
		b_np   = np.array(b_list)

		output['out_seqs'] = seq_np
		output['out_a']    = a_np
		output['out_b']    = b_np

	return output

def next_batch_base(size, max_seq_len, randomize_length=True):

	list_batch, limits = gen_list(size, max_seq_len, randomize_length)

	refs = np.copy(list_batch)
	refs[refs==-1] = 10
	refs = np.sort(refs, axis=0)
	refs[refs==10] = -1

	return list_batch, refs, limits

"""
def next_batch_trace_backup(size, steps, max_seq_len, randomize_length=True, limit_complexity=False,
	provide_target_list=False):

	n_lists = 0
	if limit_complexity:
		list_complex   = np.zeros([max_seq_len, size], dtype=np.int64)
		limits_complex = np.zeros([size], dtype=np.int64)
		traces_complex = np.zeros([steps, size], dtype=np.int64)

	are_lists_left = True
	while are_lists_left:
		sz = size - n_lists
		list_batch, limits = gen_list(sz, max_seq_len, randomize_length)
		traces = [ gen_trace(list_batch[:,i], steps, limits[i]) for i in range(sz) ]
		traces = np.stack(traces, axis=1)

		if limit_complexity:
			for i in range(sz):
				r, _, _ = exec_trace(list_batch[:,i], traces[:,i])
				if _is_sorted(r):
					list_complex[:,n_lists]   = list_batch[:,i]
					limits_complex[n_lists]   = limits[i]
					traces_complex[:,n_lists] = traces[:,i]
					n_lists += 1
		else:
			n_lists = size

		are_lists_left = n_lists < size

	if limit_complexity:
		list_batch, limits = list_complex, limits_complex
		traces             = traces_complex

	output = dict(
		seqs   = list_batch,
		limits = limits,
		ops    = traces
	)

	if provide_target_list:
		seq_list = list()
		a_list   = list()
		b_list   = list()
		for i in range(size):
			seq, a, b = exec_trace(list_batch[:,i], traces[:,i])
			seq_list.append(seq)
			a_list.append(a)
			b_list.append(b)

		seq_np = np.transpose(np.array(seq_list))
		a_np   = np.array(a_list)
		b_np   = np.array(b_list)

		output['out_seqs'] = seq_np
		output['out_a']    = a_np
		output['out_b']    = b_np

	return output
"""

if __name__ == '__main__':

	for i in range(5):
		l, length = gen_list(1, 5)
		tr = gen_trace(l[:,0], 21, length[0])
		exec_trace(l[:,0], tr, True)
		print '--------------'
		print '##############'
		print '--------------'