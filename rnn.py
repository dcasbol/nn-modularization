import tensorflow as tf
import modules.operations as om
import utils

class SortingModel(object):

	def __init__(self, input_list, max_steps, input_length, freeze=[]):

		cell = SortingCell(input_list, max_steps, input_length, freeze=freeze)

		logits, out_state = tf.nn.dynamic_rnn(cell, cell.dummy_inputs,
			dtype      = tf.float32,
			time_major = True
		)

		out_list_oh, ptr_A, ptr_B = out_state[:3]

		self._cell      = cell
		self._logits    = logits
		self._attention = tf.nn.softmax(logits)
		self._output = {
			om.LIST : cell.reshape(out_list_oh, 10),
			om.A    : cell.reshape(ptr_A, 1),
			om.B    : cell.reshape(ptr_B, 1)
		}

	@property
	def output(self):
		return self._output

	@property
	def attention(self):
		return self._attention

	@property
	def logits(self):
		return self._logits

	@property
	def trainable_variables(self):
		return tf.get_collection('model_trainable_variables')

	@property
	def trace_target_ph(self):
		return self._cell.trace_target_ph	

class SortingCell(tf.nn.rnn_cell.RNNCell):

	def __init__(self, input_list, max_steps, input_length,
		freeze=[], reuse=None):

		attention_mode = 'hard'

		super(SortingCell, self).__init__(_reuse=reuse)

		self._attention_mode = attention_mode

		self._input_list   = input_list
		self._input_length = input_length
		self._max_steps    = max_steps
		self._seq_len, self._batch_size = tf.unstack(tf.shape(input_list))

		# trainable[class] = False / True
		class_list = [om.ControllerModule, om.MovePtrA, om.MovePtrB, om.ResetPtrB, om.SwapAB]
		self._trainable = { classref : classref not in freeze for classref in class_list }

		t = self._trainable
		bm = lambda x, mask: x if mask is None else tf.boolean_mask(x, mask)
		self._instantiators = [
			lambda inputs, mask: om.MovePtrA(inputs, trainable=t[om.MovePtrA]).outputs,
			lambda inputs, mask: om.MovePtrB(inputs, trainable=t[om.MovePtrB]).outputs,
			lambda inputs, mask: om.ResetPtrB(inputs, trainable=t[om.ResetPtrB]).outputs,
			lambda inputs, mask: om.SwapAB(inputs, bm(self._input_length, mask), trainable=t[om.SwapAB]).outputs,
			lambda inputs, mask: inputs
		]
		self._num_operations = len(self._instantiators)

		# Target trace doesn't affect the functioning
		# unless it's replaced by an actual trace (used as placeholder).
		self._trace_target = -tf.ones([self._max_steps, self._batch_size], dtype=tf.int64)
		self._trace_oh     = tf.one_hot(self._trace_target, self._num_operations)

	@property
	def state_size(self):
		list_oh_size = self._seq_len*10
		ptr_A_size   = self._seq_len
		ptr_B_size   = self._seq_len
		lstm_size    = tf.nn.rnn_cell.LSTMStateTuple(100, 100)
		return list_oh_size, ptr_A_size, ptr_B_size, lstm_size, 1
	
	@property
	def output_size(self):
		return self._num_operations

	@property
	def trace_target_ph(self):
		return self._trace_target

	@property
	def dummy_inputs(self):
		return self._trace_oh

	def flatten(self, x, dim_size):
		return tf.reshape(tf.transpose(x, [1,0,2]), [self._batch_size, self._seq_len*dim_size])

	def reshape(self, x, dim_size):
		return tf.transpose(tf.reshape(x, [self._batch_size, self._seq_len, dim_size]), [1,0,2])

	def zero_state(self, batch_size, dtype):

		input_list_oh = tf.one_hot(self._input_list, 10)
		input_list_oh = self.flatten(input_list_oh, 10)

		to_pointer = lambda idx: tf.expand_dims(tf.one_hot(idx, self._seq_len, axis=0), axis=-1)

		idx_0 = tf.zeros([self._batch_size], dtype=tf.int64)
		ptr_A = self.flatten(to_pointer(idx_0), 1)
		ptr_B = self.flatten(to_pointer(idx_0 + 1), 1)

		zero_state_lstm = (tf.zeros([self._batch_size, 100]), tf.zeros([self._batch_size, 100]))
		zero_state_lstm = tf.nn.rnn_cell.LSTMStateTuple(*zero_state_lstm)

		end_flag = tf.zeros([batch_size], dtype=tf.int64)

		return input_list_oh, ptr_A, ptr_B, zero_state_lstm, end_flag

	def call(self, inputs, state):

		input_list_flat, ptr_A_flat, ptr_B_flat, lstm_state, end_flag = state

		input_list = self.reshape(input_list_flat, 10)
		ptr_A      = self.reshape(ptr_A_flat, 1)
		ptr_B      = self.reshape(ptr_B_flat, 1)

		input_state = {
			om.LIST : input_list,
			om.A    : ptr_A,
			om.B    : ptr_B
		}

		controller_module = om.ControllerModule(
			input_state, self._num_operations, self._input_length, lstm_state )
		logits   = controller_module.logits

		# Select logits that have not ended execution
		idx_flag  = tf.expand_dims(tf.cast(end_flag, tf.int32), axis=1)
		idx_batch = tf.expand_dims(tf.range(self._batch_size), axis=1)
		indices   = tf.concat([idx_flag, idx_batch], 1)
		logits   = tf.gather_nd(tf.identity([logits, inputs]), indices)

		override_quan = tf.maximum(tf.reduce_max(logits) - tf.reduce_min(logits), 1e-3) * 1.5
		trace_i = inputs * override_quan + logits

		# This works better than with selection
		all_ended = tf.reduce_min(end_flag)
		new_lstm_state = tf.cond(tf.cast(all_ended, tf.bool), lambda: lstm_state, lambda: controller_module.last_state)
		new_output     = logits

		# End execution after NOP
		op = tf.argmax(trace_i, axis=1)
		new_end_flag = tf.cast(tf.equal(op, self._num_operations-1), tf.int64)
		new_end_flag = tf.minimum(new_end_flag + end_flag, 1)
		end_mask = tf.one_hot(end_flag * self._num_operations - 1, self._num_operations, on_value=override_quan)

		trace_i = trace_i + end_mask

		assert self._attention_mode == 'hard', 'Attention mode %s is deprecated'.format(self._attention_mode)

		new_list, new_ptr_A, new_ptr_B = self._distribute_input(trace_i, input_list_flat, ptr_A_flat, ptr_B_flat)

		new_state  = new_list, new_ptr_A, new_ptr_B, new_lstm_state, new_end_flag
		return new_output, new_state

	def _distribute_input(self, logits, input_list_flat, ptr_A_flat, ptr_B_flat):

		# Merge inputs to single tensor
		l = tf.reshape(input_list_flat, [self._batch_size, self._seq_len, 10])
		A = tf.reshape(ptr_A_flat, [self._batch_size, self._seq_len, 1])
		B = tf.reshape(ptr_B_flat, [self._batch_size, self._seq_len, 1])
		input_tensor = tf.concat([l,A,B], 2)

		# Build masks to distribute computation
		batch_size = tf.shape(input_tensor)[0]
		idx_range  = tf.range(batch_size)
		op = tf.argmax(logits, axis=1)
		masks = [ tf.equal(op,i) for i in range(self._num_operations) ]
		conds = [ tf.reduce_any(m) for m in masks ]
		idxs  = [ tf.boolean_mask(idx_range, m) for m in masks ]

		# Distribute inputs among operations
		dummy_run = lambda: tf.zeros([0,self._seq_len,12])
		run       = lambda op_idx, mask: self._run_operation(op_idx, mask, input_tensor)
		if_cond   = lambda i, m, c: tf.cond(c, lambda: run(i, m), dummy_run)

		# Merge results back to a single tensor
		results    = [ if_cond(i,m,c) for i, (m, c) in enumerate(zip(masks, conds)) ]
		merged_res = tf.dynamic_stitch(idxs, results)

		# Split and flatten output
		l, A, B = tf.split(merged_res, [10,1,1], axis=2)
		flatten = lambda x: tf.reshape(x, [self._batch_size,-1])
		l, A, B = map(flatten, [l, A, B])

		return l, A, B

	def _run_operation(self, op_idx, mask, input_tensor):
		""" Ideally, time_major mode should be discarded in favor
		of batch_major mode. That would speed up the whole process """

		input_tensor = tf.boolean_mask(input_tensor, mask)
		input_tensor = tf.transpose(input_tensor, [1,0,2])
		l, A, B = tf.split(input_tensor, [10,1,1], axis=2)

		input_state = {
			om.LIST : l,
			om.A    : A,
			om.B    : B
		}

		output = self._instantiators[op_idx](input_state, mask)
		output = [ output[k] for k in [om.LIST, om.A, om.B] ]
		output_tensor = tf.concat(output, 2)
		output_tensor = tf.transpose(output_tensor, [1,0,2])

		return output_tensor
