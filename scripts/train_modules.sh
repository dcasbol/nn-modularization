for i in {3..15}
do
        for m in {ctrl,swap,ptra,ptrb,rstb}
        do
                python train.py $m --learning-rate 1e-3 --max-list-length $i --save-plot --stop-criteria "functional and quality" --input-noise
                mv training.log.dat logs/modular/noise/${m}/${m}_noise-len_${i}.log.dat
                cp -r checkpoints/${m} checkpoints/${m}_noise-bak/${m}_noise-len_${i}
        done
done


