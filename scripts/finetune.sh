#!/bin/bash

for i in {3..10}
do
	# Prepare checkpoint files
	for m in {swap,ptrA,ptrB,rstB,ctrl}
	do
		rm -r checkpoints/${m}
		cp -r checkpoints/${m}-len_${i} checkpoints/${m}
	done

	python train.py fine --max-list-length ${i} --save-plot --stop-criteria functional

	# Save plot
	mv training.log.dat logs/fine_len_${i}.log.dat
done
