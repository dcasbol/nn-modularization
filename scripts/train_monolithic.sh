
# Do five runs for each length
for i in {1..5}
do
	DIRNAME=logs/monolithic/run-${i}
	if [ ! -d "$DIRNAME" ]
	then
		RUNIDX=$i
		mkdir "$DIRNAME"
		mkdir checkpoints/FullModel-bak/run-${i}
		break
	fi
done

# Train
for i in {3..10}
do
	python train.py all --max-list-length $i --save-plot --learning-rate 1e-3 --stop-criteria "functional or slope"
	mv training.log.dat logs/monolithic/run-${RUNIDX}/len_${i}.log.dat
	cp -r checkpoints/FullModel checkpoints/FullModel-bak/run-${RUNIDX}/FullModel-len_${i}
done

